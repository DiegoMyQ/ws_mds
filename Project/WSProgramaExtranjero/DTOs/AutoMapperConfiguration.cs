﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MdsWebService;
using WSProgramaExtranjero.Constants;
using WSProgramaExtranjero.Models;

namespace WSProgramaExtranjero.DTOs
{
    public class AutoMapperConfiguration : AutoMapper.Profile
    {

        public AutoMapperConfiguration()
        {
            CreateMap<Member, ProgramaExtranjero>()
                .ForMember(dest => dest.sCodigo, mo => mo.MapFrom(src => src.MemberId.Code))
                .ForMember(dest => dest.sNombre, mo => mo.MapFrom(src => src.MemberId.Name))
                .ForMember(dest => dest.sDenominacion, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constprogramaextranjero.denominacion)).Value.ToString()
                   ))
                .ForMember(dest => dest.iCreditos, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constprogramaextranjero.creditos)).Value.ToString()
                   ))
                .ForMember(dest => dest.iDuracion, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constprogramaextranjero.duracion)).Value.ToString()
                   ))
                 .ForMember(dest => dest.sTituloExtranjero, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constprogramaextranjero.tituloextranjero)).Value.ToString()
                   ))
                .ForMember(dest => dest.sTituloEspanol, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constprogramaextranjero.tituloespanol)).Value.ToString()
                   ))
                 .ForMember(dest => dest.sTituloM, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constprogramaextranjero.titulom)).Value.ToString()
                   ))
                .ForMember(dest => dest.sTituloF, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constprogramaextranjero.titulof)).Value.ToString()
                   ))
                .ForMember(dest => dest.dVigencia, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constprogramaextranjero.vigencia)).Value.ToString()
                   ))
                .ForMember(dest => dest.dFechaInicioAcreditacion, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constprogramaextranjero.fechainicioacreditacion)).Value.ToString()
                   ))
                .ForMember(dest => dest.dFechaFinAcreditacion, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constprogramaextranjero.fechafinacreditacion)).Value.ToString()
                   ))
                 .ForMember(dest => dest.dFechaActAcreditacion, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constprogramaextranjero.fechaactacreditacion)).Value.ToString()
                   ))
                  .ForMember(dest => dest.bACreditado, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constprogramaextranjero.acreditado)).Value.ToString()
                   ))
                ;

        }


    }
}
