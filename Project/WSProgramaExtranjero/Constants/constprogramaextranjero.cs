﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSProgramaExtranjero.Constants
{
    public class constprogramaextranjero
    {
        public const string entidad = "PROGRAMA_EXTRANJERO";
        public const string name = "Name";
        public const string code = "Code";
        public const string idnivelacademico = "NIVEL_ACADEMICO";
        public const string denominacion = "DENOMINACION";
        public const string creditos = "CREDITOS";
        public const string duracion = "DURACION";
        public const string tituloextranjero = "TITULO_EXTRANJERO";
        public const string tituloespanol = "TITULO_EN_ESPANOL";
        public const string titulom = "TITULO_M";
        public const string titulof = "TITULO_F";
        public const string vigencia = "VIGENCIA";
        public const string fechainicioacreditacion = "FECHA_INICIO_ACREDITACION";
        public const string fechafinacreditacion = "FECHA_FIN_ACREDITACION";
        public const string fechaactacreditacion = "FECHA_ACT_ACREDITACION";
        public const string acreditado = "ACREDITADO";

        public const string idnivelformacion = "ID_NIVEL_DE_FORMACION";
        public const string idmetodologia = "METODOLOGIA";

        public const string idespecialidad = "ID_ESPECIALIDAD";
        public const string idcinecampoamplio = "ID_CINE_CAMPO_AMPLIO";
        public const string idcinedetallado = "ID_CINE_DETALLADO";
        public const string idcineespeficico = "ID_CINE_ESPECIFICO";
        public const string idnbc = "ID_NBC";
        public const string idtituloequivalente = "ID_TITULO_EQUIVALENTE";
        public const string idinstitucionextranjera = "ID_INSTITUCION_EXTRANJERA";
        public const string idsistemaorigen = "ID_SISTEMA_ORIGEN";
        public const string idperiocidad = "ID_PERIODICIDAD";


    }
}
