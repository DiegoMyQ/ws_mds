﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using MDS.Utilities;
using MDS.Utilities.Services;
using MdsWebService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WSProgramaExtranjero.Constants;
using WSProgramaExtranjero.Models;

namespace WSProgramaExtranjero.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class ProgramaExtranjeroController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ProgramaExtranjeroController));

        private MDSEntity _entity { get; set; }
        private List<ProgramaExtranjero> _listProgramaExtranjero { get; set; }

        private IUserService _userService;
        private readonly IMapper _mapper;

        public ProgramaExtranjeroController(IUserService userService, IMapper mapper)
        {
            this._entity = new MDSEntity(constprogramaextranjero.entidad);
            this._userService = userService;
            this._mapper = mapper; 
        }

        [HttpGet]
        public JsonResult Get()
        {
            return new JsonResult("Datos de programa extranjero");
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Post(ProgramaExtranjero programaExtranjero = null)
        {
            try
            {
                string jsonData = JsonConvert.SerializeObject(programaExtranjero);
                log.Info("Información Ingresada: " + jsonData);
                //VALIDACION DE AUTENTICACION
                var user = _userService.Authenticate(programaExtranjero.user.Username, programaExtranjero.user.Password).Result;
                if (user == null)
                {
                    log.Warn("Usuario o contraseña incorrecto");
                    return BadRequest(new { Mensaje = "Usuario o contraseña incorrecto", Errores = 1 });
                }
                   

                string searchTerm = null;

                if (programaExtranjero != null)
                {
                    IDictionary<string, object> attributes =
                    new Dictionary<string, object>();
                    attributes.Add(constprogramaextranjero.code, programaExtranjero.sCodigo);
                    attributes.Add(constprogramaextranjero.name, programaExtranjero.sNombre);
                    attributes.Add(constprogramaextranjero.denominacion, programaExtranjero.sDenominacion);


                    searchTerm = this._entity.searchTermAttributes(attributes);
                }

                this._listProgramaExtranjero = new List<ProgramaExtranjero>();

                EntityMembersGetResponse result = this._entity.GetEntity(searchTerm);

                this._listProgramaExtranjero = _mapper.Map<List<ProgramaExtranjero>>(result.EntityMembers.Members);

                for (int i = 0; i < this._listProgramaExtranjero.Count; i++)
                {
                    var member = result.EntityMembers.Members[i];

                    var educativo = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constprogramaextranjero.idnivelacademico)).Value;
                    MemberIdentifier memberEducativo = (MemberIdentifier)educativo;
                    this._listProgramaExtranjero[i].idNivelAcademico.sCodigo = memberEducativo.Code;

                    var metodologia = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constprogramaextranjero.idmetodologia)).Value;
                    MemberIdentifier memberMetodoligia = (MemberIdentifier)metodologia;
                    this._listProgramaExtranjero[i].idMetodologia.sCodigo = memberMetodoligia.Code;

                    var nivelformacion = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constprogramaextranjero.idnivelformacion)).Value;
                    MemberIdentifier memberNivelFormacion = (MemberIdentifier)nivelformacion;
                    this._listProgramaExtranjero[i].idNivelFormacion.sCodigo = memberNivelFormacion.Code;

                    var especialidad = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constprogramaextranjero.idespecialidad)).Value;
                    MemberIdentifier memberEspecialidad = (MemberIdentifier)especialidad;
                    this._listProgramaExtranjero[i].idEspecialidad.sCodigo = memberEspecialidad.Code;

                    var cinecampoamplio = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constprogramaextranjero.idcinecampoamplio)).Value;
                    MemberIdentifier memberCineCampoAmplio = (MemberIdentifier)cinecampoamplio;
                    this._listProgramaExtranjero[i].idCineCampoAmplio.sCodigo = memberCineCampoAmplio.Code;

                    var cinedeatallado = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constprogramaextranjero.idcinedetallado)).Value;
                    MemberIdentifier memberCineDetallado = (MemberIdentifier)cinedeatallado;
                    this._listProgramaExtranjero[i].idCineDetallado.sCodigo = memberCineDetallado.Code;

                    var cineespecifico = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constprogramaextranjero.idcineespeficico)).Value;
                    MemberIdentifier memberCineEspecifico = (MemberIdentifier)cineespecifico;
                    this._listProgramaExtranjero[i].idCineEspecifico.sCodigo = memberCineEspecifico.Code;

                    var nbc = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constprogramaextranjero.idnbc)).Value;
                    MemberIdentifier memberNBC = (MemberIdentifier)nbc;
                    this._listProgramaExtranjero[i].idNBC.sCodigo = memberNBC.Code;

                    var tituloequivalente = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constprogramaextranjero.idtituloequivalente)).Value;
                    MemberIdentifier memberTituloEquivalente = (MemberIdentifier)tituloequivalente;
                    this._listProgramaExtranjero[i].idTituloEquivalente.sCodigo = memberTituloEquivalente.Code;

                    var institucionextranjera = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constprogramaextranjero.idinstitucionextranjera)).Value;
                    MemberIdentifier memberInstitucionExtranjera = (MemberIdentifier)institucionextranjera;
                    this._listProgramaExtranjero[i].idInstitucionExtranjera.sCodigo = memberInstitucionExtranjera.Code;

                    var sistemaorigen = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constprogramaextranjero.idsistemaorigen)).Value;
                    MemberIdentifier memberSistemaOrigen = (MemberIdentifier)sistemaorigen;
                    this._listProgramaExtranjero[i].idSistemaOrigen.sCodigo = memberSistemaOrigen.Code;

                    var periocidad = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constprogramaextranjero.idperiocidad)).Value;
                    MemberIdentifier memberPeriocidad = (MemberIdentifier)periocidad;
                    this._listProgramaExtranjero[i].idPeriocidad.sCodigo = memberPeriocidad.Code;
                    

                }

                

                int Errores = result.OperationResult.Errors.Count;
                string Mensaje = string.Empty;
                if (Errores > 0) 
                    Mensaje = result.OperationResult.Errors.First().Description;
                else
                    Mensaje = "OK";

                log.Info("Datos de respuesta: " + JsonConvert.SerializeObject(programaExtranjero) + JsonConvert.SerializeObject(Mensaje));
                return Ok(new { Mensaje, Errores, this._listProgramaExtranjero });
            }
            catch (Exception ex)
            {
                log.Error("Error en el metodo actualizarTrazDocumento" + ex);
                log.Error("Detalle del error : " + ex.Message);
                log.Error("InnerException: " + ex.InnerException);
                log.Error("Source: " + ex.Source);
                log.Error("HelpLink: " + ex.HelpLink);
                log.Error("StackTrace: " + ex.StackTrace);
                log.Error("HResult: " + ex.HResult);
                log.Error("TargetSite: " + ex.TargetSite.Name + ex.TargetSite.Module);
                log.Error("TargetSite: " + ex.Data.Keys);
                log.Error("Tipo de excepción: " + ex.GetType().FullName);
                return BadRequest(new { Mensaje = "Error: "+ ex.Message, Errores = 1 });
            }

        }

        [AllowAnonymous]
        [Route("~/add")]
        [HttpPost]
        public ActionResult Add(ProgramaExtranjero programaExtranjero)
        {
            try
            {
                //VALIDACION DE AUTENTICACION
                string jsonData = JsonConvert.SerializeObject(programaExtranjero);
                log.Info("Información Ingresada: " + jsonData);
                //VALIDACION DE AUTENTICACION
                var user = _userService.Authenticate(programaExtranjero.user.Username, programaExtranjero.user.Password).Result;
                if (user == null)
                {
                    log.Warn("Usuario o contraseña incorrecto");
                    return BadRequest(new { Mensaje = "Usuario o contraseña incorrecto", Errores = 1 });
                }

                Member member = new Member();
                member.MemberId = new MemberIdentifier()
                {
                    Name = programaExtranjero.sNombre,
                    //Code = institucion.sCodigo,
                    MemberType = MemberType.Leaf,
                };

                IDictionary<string, object> attributes = new Dictionary<string, object>();
                attributes.Add(constprogramaextranjero.denominacion, programaExtranjero.sDenominacion);
                attributes.Add(constprogramaextranjero.creditos, programaExtranjero.iCreditos);
                attributes.Add(constprogramaextranjero.duracion, programaExtranjero.iDuracion);
                attributes.Add(constprogramaextranjero.tituloextranjero, programaExtranjero.sTituloExtranjero);
                attributes.Add(constprogramaextranjero.tituloespanol, programaExtranjero.sTituloEspanol);
                attributes.Add(constprogramaextranjero.titulom, programaExtranjero.sTituloM);
                attributes.Add(constprogramaextranjero.titulof, programaExtranjero.sTituloF);
                attributes.Add(constprogramaextranjero.vigencia, programaExtranjero.dVigencia);
                attributes.Add(constprogramaextranjero.fechainicioacreditacion, programaExtranjero.dFechaInicioAcreditacion);
                attributes.Add(constprogramaextranjero.fechafinacreditacion, programaExtranjero.dFechaFinAcreditacion);
                attributes.Add(constprogramaextranjero.fechaactacreditacion, programaExtranjero.dFechaActAcreditacion);
                attributes.Add(constprogramaextranjero.acreditado, programaExtranjero.bACreditado);


                if (programaExtranjero.idNivelAcademico != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idNivelAcademico.sCodigo;
                    attributes.Add(constprogramaextranjero.idnivelacademico, memberIdentifier);
                }

                if (programaExtranjero.idMetodologia != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idMetodologia.sCodigo;
                    attributes.Add(constprogramaextranjero.idmetodologia, memberIdentifier);
                }
                if (programaExtranjero.idNivelFormacion != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idNivelFormacion.sCodigo;
                    attributes.Add(constprogramaextranjero.idnivelformacion, memberIdentifier);
                }
                if (programaExtranjero.idEspecialidad != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idEspecialidad.sCodigo;
                    attributes.Add(constprogramaextranjero.idespecialidad, memberIdentifier);
                }
                if (programaExtranjero.idCineCampoAmplio != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idCineCampoAmplio.sCodigo;
                    attributes.Add(constprogramaextranjero.idcinecampoamplio, memberIdentifier);
                }
                if (programaExtranjero.idCineDetallado != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idCineDetallado.sCodigo;
                    attributes.Add(constprogramaextranjero.idcinedetallado, memberIdentifier);
                }
                if (programaExtranjero.idCineEspecifico != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idCineEspecifico.sCodigo;
                    attributes.Add(constprogramaextranjero.idcineespeficico, memberIdentifier);
                }
                if (programaExtranjero.idNBC != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idNBC.sCodigo;
                    attributes.Add(constprogramaextranjero.idnbc, memberIdentifier);
                }
                if (programaExtranjero.idTituloEquivalente != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idTituloEquivalente.sCodigo;
                    attributes.Add(constprogramaextranjero.idtituloequivalente, memberIdentifier);
                }
                if (programaExtranjero.idInstitucionExtranjera != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idInstitucionExtranjera.sCodigo;
                    attributes.Add(constprogramaextranjero.idinstitucionextranjera, memberIdentifier);
                }
                if (programaExtranjero.idSistemaOrigen != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idSistemaOrigen.sCodigo;
                    attributes.Add(constprogramaextranjero.idsistemaorigen, memberIdentifier);
                }
                if (programaExtranjero.idPeriocidad != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idPeriocidad.sCodigo;
                    attributes.Add(constprogramaextranjero.idperiocidad, memberIdentifier);
                }

                member.Attributes = new List<MdsWebService.Attribute>();
                member.Attributes = this._entity.setAttributes(attributes);

                var result = this._entity.AddMember(member);

                int Errores = result.OperationResult.Errors.Count;
                string Mensaje = string.Empty;
                if (Errores > 0)
                    Mensaje = result.OperationResult.Errors.First().Description;
                else
                    Mensaje = "OK";

                int code = int.Parse(result.CreatedMembers.FirstOrDefault().Code);
                log.Info("Datos de respuesta: " + JsonConvert.SerializeObject(programaExtranjero) + JsonConvert.SerializeObject(Mensaje));

                return Ok(new { Mensaje, Errores, programaExtranjero = code });


            }
            catch (Exception ex)
            {
                log.Error("Error en el metodo actualizarTrazDocumento" + ex);
                log.Error("Detalle del error : " + ex.Message);
                log.Error("InnerException: " + ex.InnerException);
                log.Error("Source: " + ex.Source);
                log.Error("HelpLink: " + ex.HelpLink);
                log.Error("StackTrace: " + ex.StackTrace);
                log.Error("HResult: " + ex.HResult);
                log.Error("TargetSite: " + ex.TargetSite.Name + ex.TargetSite.Module);
                log.Error("TargetSite: " + ex.Data.Keys);
                log.Error("Tipo de excepción: " + ex.GetType().FullName);
                return BadRequest(new { Mensaje = "Error: " + ex.Message, Errores = 1 });
            }

        }


        [AllowAnonymous]
        [Route("~/update")]
        [HttpPost]
        public ActionResult Update(ProgramaExtranjero programaExtranjero)
        {
            try
            {
                //VALIDACION DE AUTENTICACION
                string jsonData = JsonConvert.SerializeObject(programaExtranjero);
                log.Info("Información Ingresada: " + jsonData);
                //VALIDACION DE AUTENTICACION
                var user = _userService.Authenticate(programaExtranjero.user.Username, programaExtranjero.user.Password).Result;
                if (user == null)
                {
                    log.Warn("Usuario o contraseña incorrecto");
                    return BadRequest(new { Mensaje = "Usuario o contraseña incorrecto", Errores = 1 });
                }

                Member member = new Member();
                member.MemberId = new MemberIdentifier()
                {
                    Name = programaExtranjero.sNombre,
                    Code = programaExtranjero.sCodigo,
                    MemberType = MemberType.Leaf,
                };

                IDictionary<string, object> attributes = new Dictionary<string, object>();
                attributes.Add(constprogramaextranjero.denominacion, programaExtranjero.sDenominacion);
                attributes.Add(constprogramaextranjero.creditos, programaExtranjero.iCreditos);
                attributes.Add(constprogramaextranjero.duracion, programaExtranjero.iDuracion);
                attributes.Add(constprogramaextranjero.tituloextranjero, programaExtranjero.sTituloExtranjero);
                attributes.Add(constprogramaextranjero.tituloespanol, programaExtranjero.sTituloEspanol);
                attributes.Add(constprogramaextranjero.titulom, programaExtranjero.sTituloM);
                attributes.Add(constprogramaextranjero.titulof, programaExtranjero.sTituloF);
                attributes.Add(constprogramaextranjero.vigencia, programaExtranjero.dVigencia);
                attributes.Add(constprogramaextranjero.fechainicioacreditacion, programaExtranjero.dFechaInicioAcreditacion);
                attributes.Add(constprogramaextranjero.fechafinacreditacion, programaExtranjero.dFechaFinAcreditacion);
                attributes.Add(constprogramaextranjero.fechaactacreditacion, programaExtranjero.dFechaActAcreditacion);
                attributes.Add(constprogramaextranjero.acreditado, programaExtranjero.bACreditado);



                if (programaExtranjero.idNivelAcademico != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idNivelAcademico.sCodigo;
                    attributes.Add(constprogramaextranjero.idnivelacademico, memberIdentifier);
                }

                if (programaExtranjero.idMetodologia != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idMetodologia.sCodigo;
                    attributes.Add(constprogramaextranjero.idmetodologia, memberIdentifier);
                }
                if (programaExtranjero.idNivelFormacion != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idNivelFormacion.sCodigo;
                    attributes.Add(constprogramaextranjero.idnivelformacion, memberIdentifier);
                }
                if (programaExtranjero.idEspecialidad != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idEspecialidad.sCodigo;
                    attributes.Add(constprogramaextranjero.idespecialidad, memberIdentifier);
                }
                if (programaExtranjero.idCineCampoAmplio != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idCineCampoAmplio.sCodigo;
                    attributes.Add(constprogramaextranjero.idcinecampoamplio, memberIdentifier);
                }
                if (programaExtranjero.idCineDetallado != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idCineDetallado.sCodigo;
                    attributes.Add(constprogramaextranjero.idcinedetallado, memberIdentifier);
                }
                if (programaExtranjero.idCineEspecifico != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idCineEspecifico.sCodigo;
                    attributes.Add(constprogramaextranjero.idcineespeficico, memberIdentifier);
                }
                if (programaExtranjero.idNBC != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idNBC.sCodigo;
                    attributes.Add(constprogramaextranjero.idnbc, memberIdentifier);
                }
                if (programaExtranjero.idTituloEquivalente != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idTituloEquivalente.sCodigo;
                    attributes.Add(constprogramaextranjero.idtituloequivalente, memberIdentifier);
                }
                if (programaExtranjero.idInstitucionExtranjera != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idInstitucionExtranjera.sCodigo;
                    attributes.Add(constprogramaextranjero.idinstitucionextranjera, memberIdentifier);
                }
                if (programaExtranjero.idSistemaOrigen != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idSistemaOrigen.sCodigo;
                    attributes.Add(constprogramaextranjero.idsistemaorigen, memberIdentifier);
                }
                if (programaExtranjero.idPeriocidad != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = programaExtranjero.idPeriocidad.sCodigo;
                    attributes.Add(constprogramaextranjero.idperiocidad, memberIdentifier);
                }

                member.Attributes = new List<MdsWebService.Attribute>();
                member.Attributes = this._entity.setAttributes(attributes);

                var result = this._entity.UpdateMember(member);

                int Errores = result.OperationResult.Errors.Count;
                string Mensaje = string.Empty;
                if (Errores > 0)
                    Mensaje = result.OperationResult.Errors.First().Description;
                else
                    Mensaje = "OK";

                log.Info("Datos de respuesta: " + JsonConvert.SerializeObject(programaExtranjero) + JsonConvert.SerializeObject(Mensaje));

                return Ok(new { Mensaje, Errores });
            }
            catch (Exception ex)
            {
                log.Error("Error en el metodo actualizarTrazDocumento" + ex);
                log.Error("Detalle del error : " + ex.Message);
                log.Error("InnerException: " + ex.InnerException);
                log.Error("Source: " + ex.Source);
                log.Error("HelpLink: " + ex.HelpLink);
                log.Error("StackTrace: " + ex.StackTrace);
                log.Error("HResult: " + ex.HResult);
                log.Error("TargetSite: " + ex.TargetSite.Name + ex.TargetSite.Module);
                log.Error("TargetSite: " + ex.Data.Keys);
                log.Error("Tipo de excepción: " + ex.GetType().FullName);
                return BadRequest(new { Mensaje = "Error: " + ex.Message, Errores = 1 });
            }



        }


        [AllowAnonymous]
        [Route("~/delete")]
        [HttpPost]
        public ActionResult Delete(ProgramaExtranjero programaExtranjero)
        {
            try
            {
                string jsonData = JsonConvert.SerializeObject(programaExtranjero);
                log.Info("Información Ingresada: " + jsonData);
                //VALIDACION DE AUTENTICACION
                var user = _userService.Authenticate(programaExtranjero.user.Username, programaExtranjero.user.Password).Result;
                if (user == null)
                {
                    log.Warn("Usuario o contraseña incorrecto");
                    return BadRequest(new { Mensaje = "Usuario o contraseña incorrecto", Errores = 1 });
                }

                var result = this._entity.DeleteMember(programaExtranjero.sCodigo);
                int Errores = result.OperationResult.Errors.Count;
                string Mensaje = string.Empty;
                if (Errores > 0)
                    Mensaje = result.OperationResult.Errors.First().Description;
                else
                    Mensaje = "OK";

                log.Info("Datos de respuesta: " + JsonConvert.SerializeObject(programaExtranjero) + JsonConvert.SerializeObject(Mensaje));

                return Ok(new { Mensaje, Errores });

            }
            catch (Exception ex)
            {
                log.Error("Error en el metodo actualizarTrazDocumento" + ex);
                log.Error("Detalle del error : " + ex.Message);
                log.Error("InnerException: " + ex.InnerException);
                log.Error("Source: " + ex.Source);
                log.Error("HelpLink: " + ex.HelpLink);
                log.Error("StackTrace: " + ex.StackTrace);
                log.Error("HResult: " + ex.HResult);
                log.Error("TargetSite: " + ex.TargetSite.Name + ex.TargetSite.Module);
                log.Error("TargetSite: " + ex.Data.Keys);
                log.Error("Tipo de excepción: " + ex.GetType().FullName);
                return BadRequest(new { Mensaje = "Error: " + ex.Message, Errores = 1 });
            }
        }



    }
}