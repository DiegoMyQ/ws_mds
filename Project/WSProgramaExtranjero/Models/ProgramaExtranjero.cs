﻿using MDS.Utilities.Entities;
using System;

namespace WSProgramaExtranjero.Models
{
    public class ProgramaExtranjero
    {
        public User user { get; set; }

        public ProgramaExtranjero()
        {
            user = new User();
            idNivelAcademico = new Parametrica();
            idMetodologia = new Parametrica();
            idNivelFormacion = new Parametrica();
            idEspecialidad = new Parametrica();
            idCineCampoAmplio = new Parametrica();
            idCineDetallado = new Parametrica();
            idCineEspecifico = new Parametrica();
            idNBC = new Parametrica();
            idTituloEquivalente = new Parametrica();
            idInstitucionExtranjera = new Parametrica();
            idSistemaOrigen = new Parametrica();
            idPeriocidad = new Parametrica();
        }

        public string sCodigo { get; set; }
        public string sNombre { get; set; }
        public Parametrica idNivelAcademico { get; set; }

        public string sDenominacion { get; set; }
        public int iCreditos { get; set; }
        public int iDuracion { get; set; }
        public string sTituloExtranjero { get; set; }
        public string sTituloEspanol { get; set; }
        public string sTituloM { get; set; }
        public string sTituloF { get; set; }
        public DateTime? dVigencia { get; set; }
        public DateTime? dFechaInicioAcreditacion { get; set; }
        public DateTime? dFechaFinAcreditacion { get; set; }
        public DateTime? dFechaActAcreditacion { get; set; }
        public bool bACreditado { get; set; }
        public Parametrica idMetodologia { get; set; }
        public Parametrica idNivelFormacion { get; set; }
        public Parametrica idEspecialidad { get; set; }
        public Parametrica idCineCampoAmplio { get; set; }
        public Parametrica idCineDetallado { get; set; }
        public Parametrica idCineEspecifico { get; set; }
        public Parametrica idNBC { get; set; }
        public Parametrica idTituloEquivalente { get; set; }
        public Parametrica idInstitucionExtranjera { get; set; }
        public Parametrica idSistemaOrigen { get; set; }
        public Parametrica idPeriocidad { get; set; }


        public class Parametrica
        {
            public string sCodigo { get; set; }
        }

    }
}
