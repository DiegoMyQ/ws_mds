﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WSProtitulosmdm.Repositories;

namespace WSProtitulosmdm.Controllers
{
    public class ProtituloController : Controller
    {

        private readonly ProtituloRepository _protituloRepository;

        public ProtituloController(ProtituloRepository protituloRepository)
        {
            this._protituloRepository = protituloRepository;
        }

        // GET: Protitulo
        public ActionResult Index()
        {
            this._protituloRepository.GetAllProtitulos();
            return View();
        }

        // GET: Protitulo/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Protitulo/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Protitulo/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Protitulo/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Protitulo/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Protitulo/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Protitulo/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}