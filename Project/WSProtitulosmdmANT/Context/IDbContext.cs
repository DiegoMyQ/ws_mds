﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WSProtitulosmdm.Models;

namespace WSProtitulosmdm.Context
{
    public interface IDbContext
    {
         DbSet<Protitulo> Protitulo { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));

    }
}
