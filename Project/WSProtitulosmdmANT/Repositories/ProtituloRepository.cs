﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSProtitulosmdm.Context;
using WSProtitulosmdm.Models;

namespace WSProtitulosmdm.Repositories
{
    public class ProtituloRepository
    {
        private readonly IDbContext _context;


        public ProtituloRepository(IDbContext context)
        {
            _context = context;
        }

        public List<Protitulo> GetAllProtitulos()
        {
            var query = _context.Protitulo.ToList();
            return query;
        }



    }
}
