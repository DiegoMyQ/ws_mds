﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSProtitulosmdm.Models
{
    public class Protitulo
    {
        public int ID_TITULO { get; set; }
        public int ID_NIVEL_ACADEMICO { get; set; }
        public string DENOMINACION { get; set; }
        public int ID_CINE_CAMPO_AMPLIO { get; set; }
        public int ID_SISTEMA_ORIGEN { get; set; }

    }
}
