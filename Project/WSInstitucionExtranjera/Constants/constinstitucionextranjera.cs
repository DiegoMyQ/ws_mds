﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSInstitucionExtranjera.Constants
{
    public class constinstitucionextranjera
    {
        #region Constantes

        public const string entidad = "INSTITUCION_EXTRANJERA";
        public const string name = "Name";
        public const string code = "Code";
        public const string idpais = "ID_PAIS";
        public const string idniveleducativo = "ID_NIVEL_EDUCATIVO";
        public const string idsistemaorigen = "ID_SISTEMA_ORIGEN";
        public const string nombreinstitucion = "NOMBRE_INSTITUCION";
        public const string nombreespanol = "NOMBRE_EN_ESPANOL";
        public const string acreditada = "ACREDITADA";
        public const string estadofuncionamiento = "ESTADO_FUNCIONAMIENTO";
        public const string paginaweb = "PAGINA_WEB";
        public const string telefono = "TELEFONO";
        public const string informacioncontacto = "INFORMACION_DE_CONTACTO";
        public const string fechainicio = "FECHA_INICIO_ACREDITACION";
        public const string fechafin = "FECHA_FIN_ACREDITACION";
        public const string fechacreacion = "FECHA_CREACION";
        public const string fechamodificacion = "FECHA_MODIFICACION";
        public const string idciudadextranjera = "ID_CIUDAD_EXTRANJERA";

        #endregion

    }
}
