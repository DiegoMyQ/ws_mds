﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MDS.Utilities;
using MDS.Utilities.Services;
using MdsWebService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WSInstitucionExtranjera.Models;
using WSInstitucionExtranjera.Constants;
using Newtonsoft.Json;

namespace WSInstitucionExtranjera.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class InstitucionExtranjeraController : ControllerBase
    {
        private MDSEntity _entity { get; set; }
        private List<InstitucionExtranjera> _listInstitucionExtranjera { get; set; }

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(InstitucionExtranjeraController));

        private IUserService _userService;
        private readonly IMapper _mapper;


        public InstitucionExtranjeraController(IUserService userService, IMapper mapper)
        {
            this._entity = new MDSEntity(constinstitucionextranjera.entidad);
            this._userService = userService;
            this._mapper = mapper;
        }

        [HttpGet]
        public JsonResult Get()
        {
            return new JsonResult("Datos de institucion extranjera");
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Post(InstitucionExtranjera institucionExtranjera = null)
        {
            try
            {
                string jsonData = JsonConvert.SerializeObject(institucionExtranjera);
                log.Info("Información Ingresada: " + jsonData);

                //VALIDACION DE AUTENTICACION
                var user = _userService.Authenticate(institucionExtranjera.user.Username, institucionExtranjera.user.Password).Result;
            if (user == null)
                return BadRequest(new { Mensaje = "Usuario o contraseña incorrecto" , Errores = 1 });


                string searchTerm = null;

                if (institucionExtranjera != null)
                {
                    IDictionary<string, object> attributes =
                    new Dictionary<string, object>();
                    attributes.Add(constinstitucionextranjera.code, institucionExtranjera.sCodigo);
                    attributes.Add(constinstitucionextranjera.name, institucionExtranjera.sNombre);
                    attributes.Add(constinstitucionextranjera.nombreinstitucion, institucionExtranjera.sNombreInstitucion);
                    attributes.Add(constinstitucionextranjera.nombreespanol, institucionExtranjera.sNombreEspanol);
                    attributes.Add(constinstitucionextranjera.paginaweb , institucionExtranjera.sPaginaWeb);
                    attributes.Add(constinstitucionextranjera.telefono, institucionExtranjera.sTelefono);
                    attributes.Add(constinstitucionextranjera.informacioncontacto, institucionExtranjera.sInformacionContacto);
                    attributes.Add(constinstitucionextranjera.fechacreacion, institucionExtranjera.dFechaCreacion);
                    attributes.Add(constinstitucionextranjera.fechamodificacion, institucionExtranjera.dFechaModificacion);
                    attributes.Add(constinstitucionextranjera.fechainicio, institucionExtranjera.dFechaInicioAcreditacion);
                    attributes.Add(constinstitucionextranjera.fechafin, institucionExtranjera.dFechaFinAcreditacion);
                    attributes.Add(constinstitucionextranjera.acreditada, institucionExtranjera.bAcreditada);
                    attributes.Add(constinstitucionextranjera.estadofuncionamiento, institucionExtranjera.bEstadoFuncionamiento);


                    searchTerm = this._entity.searchTermAttributes(attributes);
                }

                this._listInstitucionExtranjera = new List<InstitucionExtranjera>();

                EntityMembersGetResponse result = this._entity.GetEntity(searchTerm);
               
                this._listInstitucionExtranjera = _mapper.Map< List<InstitucionExtranjera> >(result.EntityMembers.Members);


                for (int i = 0; i < this._listInstitucionExtranjera.Count; i++)
                {
                    var member = result.EntityMembers.Members[i];

                    var pais = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constinstitucionextranjera.idpais)).Value;
                    MemberIdentifier memberPais = (MemberIdentifier)pais;
                    this._listInstitucionExtranjera[i].idPais.sCodigo = memberPais.Code;
 
                    var educativo = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constinstitucionextranjera.idniveleducativo)).Value;
                    MemberIdentifier memberEducativo = (MemberIdentifier)educativo;
                    this._listInstitucionExtranjera[i].idNivelEducativo.sCodigo = memberEducativo.Code;

                    var sistema = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constinstitucionextranjera.idsistemaorigen)).Value;
                    MemberIdentifier memberSistema = (MemberIdentifier)sistema;
                    this._listInstitucionExtranjera[i].idSistemaOrigen.sCodigo = memberSistema.Code;

                    var ciudadExtranjera = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constinstitucionextranjera.idciudadextranjera)).Value;
                    MemberIdentifier memberCiudad = (MemberIdentifier)sistema;
                    this._listInstitucionExtranjera[i].idCiudadExtranjera.sCodigo = memberCiudad.Code;
                }

                int Errores = result.OperationResult.Errors.Count;
                string Mensaje = string.Empty;
                if (Errores > 0)
                    Mensaje = result.OperationResult.Errors.First().Description;
                else
                    Mensaje = "OK";

                return Ok(new{ Mensaje,Errores , this._listInstitucionExtranjera });


            }
            catch (Exception ex)
            {
                log.Error("Error en el metodo actualizarTrazDocumento" + ex);
                log.Error("Detalle del error : " + ex.Message);
                log.Error("InnerException: " + ex.InnerException);
                log.Error("Source: " + ex.Source);
                log.Error("HelpLink: " + ex.HelpLink);
                log.Error("StackTrace: " + ex.StackTrace);
                log.Error("HResult: " + ex.HResult);
                log.Error("TargetSite: " + ex.TargetSite.Name + ex.TargetSite.Module);
                log.Error("TargetSite: " + ex.Data.Keys);
                log.Error("Tipo de excepción: " + ex.GetType().FullName);
                return BadRequest(new { Mensaje = "Error: "+ ex.Message, Errores = 1 });
            }

        }

        [AllowAnonymous]
        [Route("~/add")]
        [HttpPost]
        public ActionResult Add(InstitucionExtranjera institucion)
        {
            try
            {
                //VALIDACION DE AUTENTICACION
                var user = _userService.Authenticate(institucion.user.Username, institucion.user.Password).Result;
                if (user == null)
                    return BadRequest(new { Mensaje = "Usuario o contraseña incorrecto", Errores = 1 });


                Member member = new Member();
                member.MemberId = new MemberIdentifier()
                {
                    Name = institucion.sNombre,
                    //Code = institucion.sCodigo,
                    MemberType = MemberType.Leaf,
                };
                IDictionary<string, object> attributes =
                new Dictionary<string, object>();
                attributes.Add(constinstitucionextranjera.nombreinstitucion, institucion.sNombreInstitucion);
                attributes.Add(constinstitucionextranjera.nombreespanol, institucion.sNombreEspanol);

                attributes.Add(constinstitucionextranjera.acreditada, institucion.bAcreditada);
                attributes.Add(constinstitucionextranjera.estadofuncionamiento, institucion.bEstadoFuncionamiento);
                attributes.Add(constinstitucionextranjera.paginaweb, institucion.sPaginaWeb);
                attributes.Add(constinstitucionextranjera.telefono, institucion.sTelefono);
                attributes.Add(constinstitucionextranjera.informacioncontacto, institucion.sInformacionContacto);
                attributes.Add(constinstitucionextranjera.fechafin, institucion.dFechaFinAcreditacion);
                attributes.Add(constinstitucionextranjera.fechainicio, institucion.dFechaInicioAcreditacion);
                attributes.Add(constinstitucionextranjera.fechamodificacion, institucion.dFechaModificacion);
                attributes.Add(constinstitucionextranjera.fechacreacion, institucion.dFechaCreacion);

                if (institucion.idPais != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = institucion.idPais.sCodigo;
                    attributes.Add(constinstitucionextranjera.idpais, memberIdentifier);
                }

                if (institucion.idNivelEducativo != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = institucion.idNivelEducativo.sCodigo;
                    attributes.Add(constinstitucionextranjera.idniveleducativo, memberIdentifier);
                }

                if (institucion.idSistemaOrigen != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = institucion.idSistemaOrigen.sCodigo;
                    attributes.Add(constinstitucionextranjera.idsistemaorigen, memberIdentifier);
                }

                if (institucion.idCiudadExtranjera != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = institucion.idCiudadExtranjera.sCodigo;
                    attributes.Add(constinstitucionextranjera.idciudadextranjera, memberIdentifier);
                }

                member.Attributes = new List<MdsWebService.Attribute>();
                member.Attributes = this._entity.setAttributes(attributes);
                var result = this._entity.AddMember(member);


                int Errores = result.OperationResult.Errors.Count;
                string Mensaje = string.Empty;
                if (Errores > 0)
                    Mensaje = result.OperationResult.Errors.First().Description;
                else
                    Mensaje = "OK";

                int code = int.Parse(result.CreatedMembers.FirstOrDefault().Code);

                return Ok(new { Mensaje, Errores, institucion = code  });

            }
            catch (Exception ex)
            {
                log.Error("Error en el metodo actualizarTrazDocumento" + ex);
                log.Error("Detalle del error : " + ex.Message);
                log.Error("InnerException: " + ex.InnerException);
                log.Error("Source: " + ex.Source);
                log.Error("HelpLink: " + ex.HelpLink);
                log.Error("StackTrace: " + ex.StackTrace);
                log.Error("HResult: " + ex.HResult);
                log.Error("TargetSite: " + ex.TargetSite.Name + ex.TargetSite.Module);
                log.Error("TargetSite: " + ex.Data.Keys);
                log.Error("Tipo de excepción: " + ex.GetType().FullName);
                return BadRequest(new { Mensaje = "Error: " + ex.Message, Errores = 1 });
            }
        }

        [AllowAnonymous]
        [Route("~/update")]
        [HttpPost]
        public ActionResult Update(InstitucionExtranjera institucion)
        {
            try
            {
                //VALIDACION DE AUTENTICACION
                var user = _userService.Authenticate(institucion.user.Username, institucion.user.Password).Result;
                if (user == null)
                    return BadRequest(new { Mensaje = "Usuario o contraseña incorrecto", Errores = 1 });


                Member member = new Member();
                member.MemberId = new MemberIdentifier()
                {
                    Name = institucion.sNombre,
                    Code = institucion.sCodigo,
                    MemberType = MemberType.Leaf,
                };
                IDictionary<string, object> attributes =
                new Dictionary<string, object>();

                attributes.Add(constinstitucionextranjera.nombreinstitucion, institucion.sNombreInstitucion);
                attributes.Add(constinstitucionextranjera.nombreespanol, institucion.sNombreEspanol);

                attributes.Add(constinstitucionextranjera.acreditada, institucion.bAcreditada);
                attributes.Add(constinstitucionextranjera.estadofuncionamiento, institucion.bAcreditada);
                attributes.Add(constinstitucionextranjera.paginaweb, institucion.sPaginaWeb);
                attributes.Add(constinstitucionextranjera.telefono, institucion.sTelefono);
                attributes.Add(constinstitucionextranjera.informacioncontacto, institucion.sInformacionContacto);
                attributes.Add(constinstitucionextranjera.fechafin, institucion.dFechaFinAcreditacion);
                attributes.Add(constinstitucionextranjera.fechainicio, institucion.dFechaInicioAcreditacion);
                attributes.Add(constinstitucionextranjera.fechamodificacion, institucion.dFechaModificacion);
                attributes.Add(constinstitucionextranjera.fechacreacion, institucion.dFechaCreacion);


                if (institucion.idPais != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = institucion.idPais.sCodigo;
                    attributes.Add(constinstitucionextranjera.idpais, memberIdentifier);
                }

                if (institucion.idNivelEducativo != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = institucion.idNivelEducativo.sCodigo;
                    attributes.Add(constinstitucionextranjera.idniveleducativo, memberIdentifier);
                }

                if (institucion.idSistemaOrigen != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = institucion.idSistemaOrigen.sCodigo;
                    attributes.Add(constinstitucionextranjera.idsistemaorigen, memberIdentifier);
                }

                if (institucion.idCiudadExtranjera != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = institucion.idCiudadExtranjera.sCodigo;
                    attributes.Add(constinstitucionextranjera.idciudadextranjera, memberIdentifier);
                }

                member.Attributes = new List<MdsWebService.Attribute>();
                member.Attributes = this._entity.setAttributes(attributes);
                var result = this._entity.UpdateMember(member);

                int Errores = result.OperationResult.Errors.Count;
                string Mensaje = string.Empty;
                if (Errores > 0)
                    Mensaje = result.OperationResult.Errors.First().Description;
                else
                    Mensaje = "OK";

                return Ok(new { Mensaje, Errores });

            }
            catch (Exception ex)
            {
                log.Error("Error en el metodo actualizarTrazDocumento" + ex);
                log.Error("Detalle del error : " + ex.Message);
                log.Error("InnerException: " + ex.InnerException);
                log.Error("Source: " + ex.Source);
                log.Error("HelpLink: " + ex.HelpLink);
                log.Error("StackTrace: " + ex.StackTrace);
                log.Error("HResult: " + ex.HResult);
                log.Error("TargetSite: " + ex.TargetSite.Name + ex.TargetSite.Module);
                log.Error("TargetSite: " + ex.Data.Keys);
                log.Error("Tipo de excepción: " + ex.GetType().FullName);
                return BadRequest(new { Mensaje = "Error: " + ex.Message, Errores = 1 });
            }
        }

        [AllowAnonymous]
        [Route("~/delete")]
        [HttpPost]
        public ActionResult Delete(InstitucionExtranjera institucion)
        {
            try
            {
                //VALIDACION DE AUTENTICACION
                var user = _userService.Authenticate(institucion.user.Username, institucion.user.Password).Result;
                if (user == null)
                    return BadRequest(new { Mensaje = "Usuario o contraseña incorrecto", Errores = 1 });

                var result = this._entity.DeleteMember(institucion.sCodigo);
                int Errores = result.OperationResult.Errors.Count;
                string Mensaje = string.Empty;
                if (Errores > 0)
                    Mensaje = result.OperationResult.Errors.First().Description;
                else
                    Mensaje = "OK";

                return Ok(new { Mensaje, Errores });

            }
            catch (Exception ex)
            {
                log.Error("Error en el metodo actualizarTrazDocumento" + ex);
                log.Error("Detalle del error : " + ex.Message);
                log.Error("InnerException: " + ex.InnerException);
                log.Error("Source: " + ex.Source);
                log.Error("HelpLink: " + ex.HelpLink);
                log.Error("StackTrace: " + ex.StackTrace);
                log.Error("HResult: " + ex.HResult);
                log.Error("TargetSite: " + ex.TargetSite.Name + ex.TargetSite.Module);
                log.Error("TargetSite: " + ex.Data.Keys);
                log.Error("Tipo de excepción: " + ex.GetType().FullName);
                return BadRequest(new { Mensaje = "Error: " + ex.Message, Errores = 1 });
            }
        }
    }
}