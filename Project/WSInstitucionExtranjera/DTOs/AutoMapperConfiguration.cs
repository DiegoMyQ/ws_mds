﻿using MdsWebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSInstitucionExtranjera.Models;
using WSInstitucionExtranjera.Constants;
using AutoMapper;

namespace WSInstitucionExtranjera.DTOs
{
    public class AutoMapperConfiguration: Profile
    {

        public AutoMapperConfiguration()
        {
            CreateMap<Member, InstitucionExtranjera.Pais>()
                .ForMember(dest => dest.sCodigo , mo => mo.MapFrom(
                    src =>  "pais"
                    )
                )
                ;

            CreateMap<Member, InstitucionExtranjera>()
                //.IncludeBase<Member,InstitucionExtranjera.Pais>()
                .ForMember(dest => dest.sCodigo, mo => mo.MapFrom(src => src.MemberId.Code))
                .ForMember(dest => dest.sNombre, mo => mo.MapFrom(src => src.MemberId.Name))
                .ForMember(dest => dest.sNombreInstitucion , mo => mo.MapFrom( 
                      src => src.Attributes.Single(i => i.Identifier.Name.Equals(constinstitucionextranjera.nombreinstitucion)).Value.ToString()
                    )  )
                .ForMember(dest => dest.sNombreEspanol, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constinstitucionextranjera.nombreespanol)).Value.ToString()
                   ))
                 .ForMember(dest => dest.bAcreditada, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constinstitucionextranjera.acreditada)).Value.ToString()
                   ))
                 .ForMember(dest => dest.bEstadoFuncionamiento, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constinstitucionextranjera.estadofuncionamiento)).Value.ToString()
                   ))
                 .ForMember(dest => dest.sPaginaWeb, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constinstitucionextranjera.paginaweb)).Value.ToString()
                   ))
                 .ForMember(dest => dest.sTelefono, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constinstitucionextranjera.telefono)).Value.ToString()
                   ))
                 .ForMember(dest => dest.sInformacionContacto, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constinstitucionextranjera.informacioncontacto)).Value.ToString()
                   ))
                 .ForMember(dest => dest.dFechaInicioAcreditacion, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constinstitucionextranjera.fechainicio)).Value.ToString()
                   ))
                 .ForMember(dest => dest.dFechaFinAcreditacion, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constinstitucionextranjera.fechafin)).Value.ToString()
                   ))
                  .ForMember(dest => dest.dFechaCreacion, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constinstitucionextranjera.fechacreacion)).Value.ToString()
                   ))
                  .ForMember(dest => dest.dFechaModificacion, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constinstitucionextranjera.fechamodificacion)).Value.ToString()
                   ))
                .ForAllOtherMembers(o => o.Ignore() )
            ;


            //.ForMember(dest => dest.idPais.sCodigo, mo => mo.MapFrom(
            //    src => (src.Attributes.Single(i => i.Identifier.Name.Equals(constinstitucionextranjera.idpais)).Value != null)
            //    ? "pais" : "sin pais")
            //    )



        }


    }
}
