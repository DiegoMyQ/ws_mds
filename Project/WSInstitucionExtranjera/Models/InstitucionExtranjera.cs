﻿using MDS.Utilities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSInstitucionExtranjera.Models
{
    public class InstitucionExtranjera
    {
        public User user { get; set; }

        public InstitucionExtranjera()
        {
            idPais = new Pais();
            idNivelEducativo = new NivelEducativo();
            idSistemaOrigen = new SistemaOrigen();
            idCiudadExtranjera = new CiudadExtranjera();
            user = new User();
        }

        public string sCodigo { get; set; }
        public string sNombre { get; set; }
        public Pais idPais { get; set; }
        public NivelEducativo idNivelEducativo { get; set; }
        public SistemaOrigen idSistemaOrigen { get; set; }
        public string sNombreInstitucion { get; set; }
        public string sNombreEspanol { get; set; }
        public bool? bAcreditada { get; set; }
        public bool? bEstadoFuncionamiento { get; set; }
        public string sPaginaWeb { get; set; }
        public string sTelefono { get; set; }
        public string sInformacionContacto { get; set; }

        public DateTime? dFechaInicioAcreditacion { get; set; }
        public DateTime? dFechaFinAcreditacion { get; set; }
        public DateTime? dFechaCreacion { get; set; }
        public DateTime? dFechaModificacion { get; set; }
        public CiudadExtranjera idCiudadExtranjera { get; set; }

        public class Pais
        {
            public string sCodigo { get; set; }
        }
        public class NivelEducativo
        {
            public string sCodigo { get; set; }
        }

        public class SistemaOrigen
        {
            public string sCodigo { get; set; }
        }

        public class CiudadExtranjera
        {
            public string sCodigo { get; set; }
        }


    }


}
