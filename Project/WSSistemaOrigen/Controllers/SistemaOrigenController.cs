﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MDS.Utilities;
using MdsWebService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WSSistemaOrigen.Models;

namespace WSSistemaOrigen.Controllers
{
    [Produces("application/json")]
    [Route("[controller]")]
    [ApiController]
    public class SistemaOrigenController : ControllerBase
    {
        private MDSEntity _entity { get; set; }
        private List<SistemaOrigen> _listNivelAc { get; set; }


        #region Constantes

        private const string entidad = "SISTEMA_ORIGEN";
        private const string name = "Name";
        private const string code = "Code";
        private const string descripcion = "DESCRIPCION";
        private const string fechainicio = "FECHA_INICIO";
        private const string fechafin = "FECHA_FIN";

        #endregion

        public SistemaOrigenController()
        {
            this._entity = new MDSEntity(entidad);
        }

        [HttpGet]
        public JsonResult Get()
        {
            return new JsonResult("Datos de sistema de origen");
        }


        [HttpPost]
        public List<SistemaOrigen> Post(SistemaOrigen sistemaOrigen = null)
        {
            string searchTerm = null;

            if (sistemaOrigen != null)
            {
                IDictionary<string, object> attributes =
                new Dictionary<string, object>();
                attributes.Add(code, sistemaOrigen.sCodigo);
                attributes.Add(name, sistemaOrigen.sNombre);
                attributes.Add(descripcion, sistemaOrigen.sDescripcion);
                attributes.Add(fechainicio, sistemaOrigen.dFechaInicio);
                attributes.Add(fechafin, sistemaOrigen.dFechaFin);

                searchTerm = this._entity.searchTermAttributes(attributes);
            }

            List<Member> result = this._entity.GetEntity(searchTerm);
            this._listNivelAc = new List<SistemaOrigen>();
            foreach (Member member in result)
            {
                SistemaOrigen nivelSistemaOrigen = new SistemaOrigen();
                nivelSistemaOrigen.sNombre = member.MemberId.Name;
                nivelSistemaOrigen.sCodigo = member.MemberId.Code;
                nivelSistemaOrigen.sDescripcion = member.Attributes.Single(i => i.Identifier.Name.Equals(descripcion)).Value.ToString();

                var dInicio = member.Attributes.Single(i => i.Identifier.Name.Equals(fechainicio)).Value;
                var dFin = member.Attributes.Single(i => i.Identifier.Name.Equals(fechafin)).Value;
                nivelSistemaOrigen.dFechaInicio = dInicio == null ? null : (DateTime?)DateTime.Parse(dInicio.ToString());
                nivelSistemaOrigen.dFechaFin = dFin == null ? null : (DateTime?)DateTime.Parse(dFin.ToString());

                this._listNivelAc.Add(nivelSistemaOrigen);
            }

            return this._listNivelAc;
        }

        [Route("~/sistemaorigen/add")]
        [HttpPost]
        public string Add(SistemaOrigen sistemaOrigen)
        {
            Member member = new Member();
            member.MemberId = new MemberIdentifier()
            {
                Name = sistemaOrigen.sNombre,
                Code = sistemaOrigen.sCodigo,
                MemberType = MemberType.Leaf,
            };
            IDictionary<string, object> attributes =
            new Dictionary<string, object>();
            attributes.Add(descripcion, sistemaOrigen.sDescripcion);
            attributes.Add(fechainicio, sistemaOrigen.dFechaInicio);
            attributes.Add(fechafin, sistemaOrigen.dFechaFin);
            member.Attributes = new List<MdsWebService.Attribute>();
            member.Attributes = this._entity.setAttributes(attributes);
            var result = this._entity.AddMember(member);

            return result;
        }

        [Route("~/sistemaorigen/update")]
        [HttpPost]
        public string Update(SistemaOrigen sistemaOrigen)
        {
            Member member = new Member();
            member.MemberId = new MemberIdentifier()
            {
                Name = sistemaOrigen.sNombre,
                Code = sistemaOrigen.sCodigo,
                MemberType = MemberType.Leaf,
            };
            IDictionary<string, object> attributes =
            new Dictionary<string, object>();
            attributes.Add(descripcion, sistemaOrigen.sDescripcion);
            attributes.Add(fechainicio, sistemaOrigen.dFechaInicio);
            attributes.Add(fechafin, sistemaOrigen.dFechaFin);
            member.Attributes = new List<MdsWebService.Attribute>();
            member.Attributes = this._entity.setAttributes(attributes);
            var result = this._entity.UpdateMember(member);
            return result;
        }

        [Route("~/sistemaorigen/delete")]
        [HttpPost]
        public string Delete(SistemaOrigen sistemaOrigen)
        {
            var result = this._entity.DeleteMember(sistemaOrigen.sCodigo);
            return result;
        }




    }
}