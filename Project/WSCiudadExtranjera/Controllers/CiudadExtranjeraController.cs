﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MDS.Utilities;
using MdsWebService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WSCiudadExtranjera.Models;

namespace WSCiudadExtranjera.Controllers
{
    [Produces("application/json")]
    [Route("[controller]")]
    [ApiController]
    public class CiudadExtranjeraController : ControllerBase
    {
        private MDSEntity _entity { get; set; }
        private List<CiudadExtranjera> _listNivelAc { get; set; }

        #region Constantes

        private const string entidad = "MUNICIPIO_EXTRANJERO";
        private const string name = "Name";
        private const string code = "Code";
        private const string ciudadextranjera = "CIUDAD_EXTRANJERA";
        private const string fechainicio = "FECHA_INICIO";
        private const string fechafin = "FECHA_FIN";
        private const string idpais = "ID_PAIS";

        #endregion

        public CiudadExtranjeraController()
        {
            this._entity = new MDSEntity(entidad);
        }

        [HttpGet]
        public JsonResult Get()
        {
            return new JsonResult("Datos de ciudad extranjera");
        }

        [HttpPost]
        public List<CiudadExtranjera> Post(CiudadExtranjera ciudadExtranjera = null)
        {
            string searchTerm = null;

            if (ciudadExtranjera != null)
            {
                IDictionary<string, object> attributes =
                new Dictionary<string, object>();
                attributes.Add(code, ciudadExtranjera.sCodigo);
                attributes.Add(name, ciudadExtranjera.sNombre);
                attributes.Add(ciudadextranjera, ciudadExtranjera.sCiudadExtranjera);
                attributes.Add(fechainicio, ciudadExtranjera.dFechaInicio);
                attributes.Add(fechafin, ciudadExtranjera.dFechaFin);

                searchTerm = this._entity.searchTermAttributes(attributes);
            }

            List<Member> result = this._entity.GetEntity(searchTerm);
            this._listNivelAc = new List<CiudadExtranjera>();
            foreach (Member member in result)
            {
                CiudadExtranjera nivelCiudad = new CiudadExtranjera();
                nivelCiudad.sNombre = member.MemberId.Name;
                nivelCiudad.sCodigo = member.MemberId.Code;
                nivelCiudad.sCiudadExtranjera = member.Attributes.Single(i => i.Identifier.Name.Equals(ciudadextranjera)).Value.ToString();

                var dInicio = member.Attributes.Single(i => i.Identifier.Name.Equals(fechainicio)).Value;
                var dFin = member.Attributes.Single(i => i.Identifier.Name.Equals(fechafin)).Value;
                nivelCiudad.dFechaInicio = dInicio == null ? null : (DateTime?)DateTime.Parse(dInicio.ToString());
                nivelCiudad.dFechaFin = dFin == null ? null : (DateTime?)DateTime.Parse(dFin.ToString());

                var pais = member.Attributes.Single(i => i.Identifier.Name.Equals(idpais)).Value;
                MemberIdentifier memberPais = (MemberIdentifier)pais;
                nivelCiudad.idPais.sCodigo = memberPais.Code;

                this._listNivelAc.Add(nivelCiudad);
            }

            return this._listNivelAc;
        }


        [Route("~/ciudadextranjera/add")]
        [HttpPost]
        public string Add(CiudadExtranjera ciudadExtranjera)
        {
            Member member = new Member();
            member.MemberId = new MemberIdentifier()
            {
                Name = ciudadExtranjera.sNombre,
                Code = ciudadExtranjera.sCodigo,
                MemberType = MemberType.Leaf,
            };
            IDictionary<string, object> attributes =
            new Dictionary<string, object>();
            attributes.Add(ciudadextranjera, ciudadExtranjera.sCiudadExtranjera);
            attributes.Add(fechainicio, ciudadExtranjera.dFechaInicio);
            attributes.Add(fechafin, ciudadExtranjera.dFechaFin);

            if (ciudadExtranjera.idPais != null)
            {
                MemberIdentifier memberIdentifier = new MemberIdentifier();
                memberIdentifier.Code = ciudadExtranjera.idPais.sCodigo;
                attributes.Add(idpais, memberIdentifier);
            }

            member.Attributes = new List<MdsWebService.Attribute>();
            member.Attributes = this._entity.setAttributes(attributes);
            var result = this._entity.AddMember(member);

            return result;
        }

        [Route("~/ciudadextranjera/update")]
        [HttpPost]
        public string Update(CiudadExtranjera ciudadExtranjera)
        {
            Member member = new Member();
            member.MemberId = new MemberIdentifier()
            {
                Name = ciudadExtranjera.sNombre,
                Code = ciudadExtranjera.sCodigo,
                MemberType = MemberType.Leaf,
            };
            IDictionary<string, object> attributes =
            new Dictionary<string, object>();
            attributes.Add(ciudadextranjera, ciudadExtranjera.sCiudadExtranjera);
            attributes.Add(fechainicio, ciudadExtranjera.dFechaInicio);
            attributes.Add(fechafin, ciudadExtranjera.dFechaFin);

            if (ciudadExtranjera.idPais != null)
            {
                MemberIdentifier memberIdentifier = new MemberIdentifier();
                memberIdentifier.Code = ciudadExtranjera.idPais.sCodigo;
                attributes.Add(idpais, memberIdentifier);
            }

            member.Attributes = new List<MdsWebService.Attribute>();
            member.Attributes = this._entity.setAttributes(attributes);
            var result = this._entity.UpdateMember(member);
            return result;
        }

        [Route("~/ciudadextranjera/delete")]
        [HttpPost]
        public string Delete(CiudadExtranjera ciudadExtranjera)
        {
            var result = this._entity.DeleteMember(ciudadExtranjera.sCodigo);
            return result;
        }



    }
}