﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSCiudadExtranjera.Models
{
    public class CiudadExtranjera
    {
        public CiudadExtranjera()
        {
            idPais = new Pais();
        }

        public string sCodigo { get; set; }
        public string sNombre { get; set; }
        public string sCiudadExtranjera { get; set; }
        public DateTime? dFechaInicio { get; set; }
        public DateTime? dFechaFin { get; set; }
        public Pais idPais { get; set; }

        public class Pais
        {
            public string sCodigo { get; set; }

        }

    }
}
