﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MDS.Utilities;
using MDS.Utilities.Services;
using MdsWebService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WSPais.Models;

namespace WSPais.Controllers
{
    [Produces("application/json")]
    [Route("[controller]")]
    [ApiController]
    public class PaisController : ControllerBase
    {
        private MDSEntity _entity { get; set; }
        private List<Pais> _listPais { get; set; }
        private IUserService _userService;


        #region Constantes

        private const string entidad = "PAIS";
        private const string name = "Name";
        private const string code = "Code";
        private const string paisS = "PAIS";
        private const string fechainicio = "FECHA_INICIO";
        private const string fechafin = "FECHA_FIN";
        private const string idpais = "ID_PAIS";
        #endregion


        public PaisController(IUserService userService)
        {
            this._entity = new MDSEntity(entidad);
            this._userService = userService;
        }


        [HttpGet]
        public JsonResult Get()
        {
            return new JsonResult("Datos de pais");
        }

        [HttpPost]
        public ActionResult Post(Pais pais = null)
        {
            //VALIDACION DE AUTENTICACION
            var user = _userService.Authenticate(pais.user.Username, pais.user.Password).Result;
            if (user == null)
                return BadRequest(new { Mensaje = "Usuario o contraseña incorrecto", Errores = 1 });


            string searchTerm = null;

            if (pais != null)
            {
                IDictionary<string, object> attributes =
                new Dictionary<string, object>();
                attributes.Add(code, pais.sCodigo);
                attributes.Add(name, pais.sNombre);
                attributes.Add(paisS, pais.sPais);
                attributes.Add(fechainicio, pais.dFechaInicio);
                attributes.Add(fechafin, pais.dFechaFin);
                attributes.Add(idpais, pais.idPais);

                searchTerm = this._entity.searchTermAttributes(attributes);
            }
            var result = this._entity.GetEntity(searchTerm);
            List<Member> resultR = result.EntityMembers.Members;
            this._listPais = new List<Pais>();
            foreach (Member member in resultR)
            {
                Pais nivelPais= new Pais();
                nivelPais.sNombre = member.MemberId.Name;
                nivelPais.sCodigo = member.MemberId.Code;
                nivelPais.sPais = member.Attributes.Single(i => i.Identifier.Name.Equals(paisS)).Value.ToString();

                var dInicio = member.Attributes.Single(i => i.Identifier.Name.Equals(fechainicio)).Value;
                var dFin = member.Attributes.Single(i => i.Identifier.Name.Equals(fechafin)).Value;
                nivelPais.dFechaInicio = dInicio == null ? null : (DateTime?)DateTime.Parse(dInicio.ToString());
                nivelPais.dFechaFin = dFin == null ? null : (DateTime?)DateTime.Parse(dFin.ToString());
                
                var idPAIS = member.Attributes.Single(i => i.Identifier.Name.Equals(idpais)).Value.ToString();
                nivelPais.idPais = dFin == null ? null : (decimal?)decimal.Parse(idPAIS.ToString());

                this._listPais.Add(nivelPais);
            }

            int Errores = result.OperationResult.Errors.Count;
            string Mensaje = string.Empty;
            if (Errores > 0)
                Mensaje = result.OperationResult.Errors.First().Description;

            return Ok(new { Mensaje, Errores, this._listPais });
        }

        [Route("~/pais/add")]
        [HttpPost]
        public string Add(Pais pais)
        {
            Member member = new Member();
            member.MemberId = new MemberIdentifier()
            {
                Name = pais.sNombre,
                Code = pais.sCodigo,
                MemberType = MemberType.Leaf,
            };
            IDictionary<string, object> attributes =
            new Dictionary<string, object>();
            attributes.Add(paisS, pais.sPais);
            attributes.Add(fechainicio, pais.dFechaInicio);
            attributes.Add(fechafin, pais.dFechaFin);
            attributes.Add(idpais, pais.idPais);

            member.Attributes = new List<MdsWebService.Attribute>();
            member.Attributes = this._entity.setAttributes(attributes);
            var result = this._entity.AddMember(member);
            return result;
        }

        [Route("~/pais/update")]
        [HttpPost]
        public string Update(Pais pais)
        {
            Member member = new Member();
            member.MemberId = new MemberIdentifier()
            {
                Name = pais.sNombre,
                Code = pais.sCodigo,
                MemberType = MemberType.Leaf,
            };
            IDictionary<string, object> attributes =
            new Dictionary<string, object>();
            attributes.Add(paisS, pais.sPais);
            attributes.Add(fechainicio, pais.dFechaInicio);
            attributes.Add(fechafin, pais.dFechaFin);
            attributes.Add(idpais, pais.idPais);
             
            member.Attributes = new List<MdsWebService.Attribute>();
            member.Attributes = this._entity.setAttributes(attributes);
            var result = this._entity.UpdateMember(member);
            return result;
        }

        [Route("~/pais/delete")]
        [HttpPost]
        public string Delete(Pais pais)
        {
            var result = this._entity.DeleteMember(pais.sCodigo);
            return result;
        }

    }
}