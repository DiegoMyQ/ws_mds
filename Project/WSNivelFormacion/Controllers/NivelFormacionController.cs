﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MDS.Utilities;
using MdsWebService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WSNivelFormacion.Models;

namespace WSNivelFormacion.Controllers
{
    [Produces("application/json")]
    [Route("[controller]")]
    [ApiController]
    public class NivelFormacionController : ControllerBase
    {
        private MDSEntity _entity { get; set; }
        private List<NivelFormacion> _listNivelAc { get; set; }

        #region Constantes

        private const string entidad = "NIVEL_DE_FORMACION";
        private const string name = "Name";
        private const string code = "Code";
        private const string descripcion = "DESCRIPCION";
        private const string fechainicio = "FECHA_INICIO";
        private const string fechafin = "FECHA_FIN";
        private const string idnivelacademico = "ID_NIVEL_ACADEMICO";

        #endregion

        public NivelFormacionController()
        {
            this._entity = new MDSEntity(entidad);

        }

        [HttpGet]
        public JsonResult Get()
        {
            return new JsonResult("Datos de nivel formacion");
        }

        [HttpPost]
        public List<NivelFormacion> Post(NivelFormacion formacion = null)
        {
            string searchTerm = null;

            if (formacion != null)
            {
                IDictionary<string, object> attributes =
                new Dictionary<string, object>();
                attributes.Add(code, formacion.sCodigo);
                attributes.Add(name, formacion.sNombre);
                attributes.Add(descripcion, formacion.sDescripcion);
                attributes.Add(fechainicio, formacion.dFechaInicio);
                attributes.Add(fechafin, formacion.dFechaFin);

                searchTerm = this._entity.searchTermAttributes(attributes);
            }

            List<Member> result = this._entity.GetEntity(searchTerm);
            this._listNivelAc = new List<NivelFormacion>();
            foreach (Member member in result)
            {
                NivelFormacion nivelFormacion = new NivelFormacion();
                nivelFormacion.sNombre = member.MemberId.Name;
                nivelFormacion.sCodigo = member.MemberId.Code;
                nivelFormacion.sDescripcion = member.Attributes.Single(i => i.Identifier.Name.Equals(descripcion)).Value.ToString();

                var dInicio = member.Attributes.Single(i => i.Identifier.Name.Equals(fechainicio)).Value;
                var dFin = member.Attributes.Single(i => i.Identifier.Name.Equals(fechafin)).Value;
                nivelFormacion.dFechaInicio = dInicio == null ? null : (DateTime?)DateTime.Parse(dInicio.ToString());
                nivelFormacion.dFechaFin = dFin == null ? null : (DateTime?)DateTime.Parse(dFin.ToString());

                var educativo = member.Attributes.Single(i => i.Identifier.Name.Equals(idnivelacademico)).Value;
                MemberIdentifier memberEducativo = (MemberIdentifier)educativo;
                nivelFormacion.idNivelAcademico.sCodigo = memberEducativo.Code;

                this._listNivelAc.Add(nivelFormacion);
            }

            return this._listNivelAc;
        }

        [Route("~/nivelformacion/add")]
        [HttpPost]
        public string Add(NivelFormacion nivelFormacion)
        {
            Member member = new Member();
            member.MemberId = new MemberIdentifier()
            {
                Name = nivelFormacion.sNombre,
                Code = nivelFormacion.sCodigo,
                MemberType = MemberType.Leaf,
            };
            IDictionary<string, object> attributes =
            new Dictionary<string, object>();
            attributes.Add(descripcion, nivelFormacion.sDescripcion);
            attributes.Add(fechainicio, nivelFormacion.dFechaInicio);
            attributes.Add(fechafin, nivelFormacion.dFechaFin);

            if (nivelFormacion.idNivelAcademico != null)
            {
                MemberIdentifier memberIdentifier = new MemberIdentifier();
                memberIdentifier.Code = nivelFormacion.idNivelAcademico.sCodigo;
                attributes.Add(idnivelacademico, memberIdentifier);
            }

            member.Attributes = new List<MdsWebService.Attribute>();
            member.Attributes = this._entity.setAttributes(attributes);
            var result = this._entity.AddMember(member);

            return result;
        }

        [Route("~/nivelformacion/update")]
        [HttpPost]
        public string Update(NivelFormacion nivelFormacion)
        {
            Member member = new Member();
            member.MemberId = new MemberIdentifier()
            {
                Name = nivelFormacion.sNombre,
                Code = nivelFormacion.sCodigo,
                MemberType = MemberType.Leaf,
            };
            IDictionary<string, object> attributes =
            new Dictionary<string, object>();
            attributes.Add(descripcion, nivelFormacion.sDescripcion);
            attributes.Add(fechainicio, nivelFormacion.dFechaInicio);
            attributes.Add(fechafin, nivelFormacion.dFechaFin);

            if (nivelFormacion.idNivelAcademico != null)
            {
                MemberIdentifier memberIdentifier = new MemberIdentifier();
                memberIdentifier.Code = nivelFormacion.idNivelAcademico.sCodigo;
                attributes.Add(idnivelacademico, memberIdentifier);
            }

            member.Attributes = new List<MdsWebService.Attribute>();
            member.Attributes = this._entity.setAttributes(attributes);
            var result = this._entity.UpdateMember(member);
            return result;
        }

        [Route("~/nivelformacion/delete")]
        [HttpPost]
        public string Delete(NivelFormacion nivelFormacion)
        {
            var result = this._entity.DeleteMember(nivelFormacion.sCodigo);
            return result;
        }

    }
}