﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSNivelFormacion.Models
{
    public class NivelFormacion
    {
        public NivelFormacion()
        {
            idNivelAcademico = new NivelAcademico();
        }

        public string sCodigo { get; set; }
        public string sNombre { get; set; }
        public string sDescripcion { get; set; }
        public DateTime? dFechaInicio { get; set; }
        public DateTime? dFechaFin { get; set; }
        public NivelAcademico idNivelAcademico { get; set; }

        public class NivelAcademico
        {
            public string sCodigo { get; set; }
        }


    }
}
