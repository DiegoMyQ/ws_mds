﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSTitulo.Models
{
    public class Titulo
    {
        public Titulo()
        {
            idNivelAcademico = new NivelAcademico();
            idCineCampoAmplio = new CineCampoAmplio();
            idSistemaOrigen = new SistemaOrigen();
        }

        public string sCodigo { get; set; }
        public string sNombre { get; set; }
        public string sDenominacion { get; set; }

        public NivelAcademico idNivelAcademico { get; set; }
        public CineCampoAmplio idCineCampoAmplio { get; set; }
        public SistemaOrigen idSistemaOrigen { get; set; }

        public class NivelAcademico
        {
            public string sCodigo { get; set; }
        }

        public class CineCampoAmplio
        {
            public string sCodigo { get; set; }
        }

        public class SistemaOrigen
        {
            public string sCodigo { get; set; }
        }

    }
}
