﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MDS.Utilities;
using MdsWebService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WSTitulo.Models;

namespace WSTitulo.Controllers
{
    [Produces("application/json")]
    [Route("[controller]")]
    [ApiController]
    public class TituloController : ControllerBase
    {

        private MDSEntity _entity { get; set; }
        private List<Titulo> _listNivelAc { get; set; }

        #region Constantes

        private const string entidad = "TITULOS";
        private const string name = "Name";
        private const string code = "Code";
        private const string denominacion = "DENOMINACION"; 
        private const string idnivelacademico = "ID_NIVEL_ACADEMICO";
        private const string idcinecampoamplio = "ID_CINE_CAMPO_AMPLIO";
        private const string idsistemaorigen = "ID_SISTEMA_ORIGEN";


        #endregion

        public TituloController()
        {
            this._entity = new MDSEntity(entidad);
        }

        [HttpGet]
        public JsonResult Get()
        {
            return new JsonResult("Datos de titulos");
        }

        [HttpPost]
        public List<Titulo> Post(Titulo titulo = null)
        {
            string searchTerm = null;

            if (titulo != null)
            {
                IDictionary<string, object> attributes =
                new Dictionary<string, object>();
                attributes.Add(code, titulo.sCodigo);
                attributes.Add(name, titulo.sNombre);
                attributes.Add(denominacion, titulo.sDenominacion);

                searchTerm = this._entity.searchTermAttributes(attributes);
            }

            List<Member> result = this._entity.GetEntity(searchTerm);
            this._listNivelAc = new List<Titulo>();
            foreach (Member member in result)
            {
                Titulo nivelTitulo = new Titulo();
                nivelTitulo.sNombre = member.MemberId.Name;
                nivelTitulo.sCodigo = member.MemberId.Code;
                nivelTitulo.sDenominacion = member.Attributes.Single(i => i.Identifier.Name.Equals(denominacion)).Value.ToString();


                var educativo = member.Attributes.Single(i => i.Identifier.Name.Equals(idnivelacademico)).Value;
                MemberIdentifier memberEducativo = (MemberIdentifier)educativo;
                nivelTitulo.idNivelAcademico.sCodigo = memberEducativo.Code;

                var cineAmplio = member.Attributes.Single(i => i.Identifier.Name.Equals(idcinecampoamplio)).Value;
                MemberIdentifier memberCineAmplio = (MemberIdentifier)cineAmplio;
                nivelTitulo.idCineCampoAmplio.sCodigo = memberCineAmplio.Code;

                var sistemaOrigen = member.Attributes.Single(i => i.Identifier.Name.Equals(idsistemaorigen)).Value;
                MemberIdentifier memberSistemaOrigen = (MemberIdentifier)sistemaOrigen;
                nivelTitulo.idSistemaOrigen.sCodigo = memberSistemaOrigen.Code;

                this._listNivelAc.Add(nivelTitulo);
            }

            return this._listNivelAc;
        }


        [Route("~/titulo/add")]
        [HttpPost]
        public string Add(Titulo titulo)
        {
            Member member = new Member();
            member.MemberId = new MemberIdentifier()
            {
                Name = titulo.sNombre,
                Code = titulo.sCodigo,
                MemberType = MemberType.Leaf,
            };
            IDictionary<string, object> attributes =
            new Dictionary<string, object>();
            attributes.Add(denominacion, titulo.sDenominacion);

            if (titulo.idNivelAcademico != null)
            {
                MemberIdentifier memberIdentifier = new MemberIdentifier();
                memberIdentifier.Code = titulo.idNivelAcademico.sCodigo;
                attributes.Add(idnivelacademico, memberIdentifier);
            }
            if (titulo.idCineCampoAmplio != null)
            {
                MemberIdentifier memberIdentifier = new MemberIdentifier();
                memberIdentifier.Code = titulo.idCineCampoAmplio.sCodigo;
                attributes.Add(idcinecampoamplio, memberIdentifier);
            }
            if (titulo.idSistemaOrigen != null)
            {
                MemberIdentifier memberIdentifier = new MemberIdentifier();
                memberIdentifier.Code = titulo.idSistemaOrigen.sCodigo;
                attributes.Add(idsistemaorigen, memberIdentifier);
            }

            member.Attributes = new List<MdsWebService.Attribute>();
            member.Attributes = this._entity.setAttributes(attributes);
            var result = this._entity.AddMember(member);

            return result;
        }

        [Route("~/titulo/update")]
        [HttpPost]
        public string Update(Titulo titulo)
        {
            Member member = new Member();
            member.MemberId = new MemberIdentifier()
            {
                Name = titulo.sNombre,
                Code = titulo.sCodigo,
                MemberType = MemberType.Leaf,
            };
            IDictionary<string, object> attributes =
            new Dictionary<string, object>();
            attributes.Add(denominacion, titulo.sDenominacion);

            if (titulo.idNivelAcademico != null)
            {
                MemberIdentifier memberIdentifier = new MemberIdentifier();
                memberIdentifier.Code = titulo.idNivelAcademico.sCodigo;
                attributes.Add(idnivelacademico, memberIdentifier);
            }
            if (titulo.idCineCampoAmplio != null)
            {
                MemberIdentifier memberIdentifier = new MemberIdentifier();
                memberIdentifier.Code = titulo.idCineCampoAmplio.sCodigo;
                attributes.Add(idcinecampoamplio, memberIdentifier);
            }
            if (titulo.idSistemaOrigen != null)
            {
                MemberIdentifier memberIdentifier = new MemberIdentifier();
                memberIdentifier.Code = titulo.idSistemaOrigen.sCodigo;
                attributes.Add(idsistemaorigen, memberIdentifier);
            }

            member.Attributes = new List<MdsWebService.Attribute>();
            member.Attributes = this._entity.setAttributes(attributes);
            var result = this._entity.UpdateMember(member);
            return result;
        }


        [Route("~/titulo/delete")]
        [HttpPost]
        public string Delete(Titulo titulo)
        {
            var result = this._entity.DeleteMember(titulo.sCodigo);
            return result;
        }



    }
}