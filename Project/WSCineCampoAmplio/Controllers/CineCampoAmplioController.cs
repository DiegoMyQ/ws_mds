﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MDS.Utilities;
using MDS.Utilities.Entities;
using MDS.Utilities.Services;
using MdsWebService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WSCineCampoAmplio.Models;

namespace WSCineCampoAmplio.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("[controller]")]
    [ApiController]
    public class CineCampoAmplioController : ControllerBase
    {
        private MDSEntity _entity { get; set; }
        private List<CineCampoAmplio> _listNivelAc { get; set; }

        #region Constantes

        private const string entidad = "CINE_CAMPO_AMPLIO";
        private const string name = "Name";
        private const string code = "Code";
        private const string descripcion = "DESCRIPCION";
        private const string fechainicio = "FECHA_INICIO";
        private const string fechafin = "FECHA_FIN";

        #endregion


        //public CineCampoAmplioController()
        //{
        //    this._entity = new MDSEntity(entidad);
        //}

        private IUserService _userService;
        public CineCampoAmplioController(IUserService userService)
        {
            _userService = userService;
            this._entity = new MDSEntity(entidad);
        }

        public class Prueba
        {
            public MDS.Utilities.Entities.User user { get; set; }

        }
  
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate(Prueba userParam, string diego)
        {
            var user = await _userService.Authenticate(userParam.user.Username, userParam.user.Password);
            if (user == null)
                return BadRequest(new { message = "Usuario o contraseña es incorrecto" });
            return Ok(user);
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var users = await _userService.GetAll();
            return Ok(users);
        }

        public JsonResult Get()
        {
            return new JsonResult("Datos de cine campo amplio");
        }


        [HttpPost]
        public List<CineCampoAmplio> Post(CineCampoAmplio cineCampoAmplio = null)
        {
            string searchTerm = null;

            if (cineCampoAmplio != null)
            {
                IDictionary<string, object> attributes =
                new Dictionary<string, object>();
                attributes.Add(code, cineCampoAmplio.sCodigo);
                attributes.Add(name, cineCampoAmplio.sNombre);
                attributes.Add(descripcion, cineCampoAmplio.sDescripcion);
                attributes.Add(fechainicio, cineCampoAmplio.dFechaInicio);
                attributes.Add(fechafin, cineCampoAmplio.dFechaFin);

                searchTerm = this._entity.searchTermAttributes(attributes);
            }

            List<Member> result = this._entity.GetEntity(searchTerm).EntityMembers.Members;
            this._listNivelAc = new List<CineCampoAmplio>();
            foreach (Member member in result)
            {
                CineCampoAmplio nivelCine = new CineCampoAmplio();
                nivelCine.sNombre = member.MemberId.Name;
                nivelCine.sCodigo = member.MemberId.Code;
                nivelCine.sDescripcion = member.Attributes.Single(i => i.Identifier.Name.Equals(descripcion)).Value.ToString();

                var dInicio = member.Attributes.Single(i => i.Identifier.Name.Equals(fechainicio)).Value;
                var dFin = member.Attributes.Single(i => i.Identifier.Name.Equals(fechafin)).Value;
                nivelCine.dFechaInicio = dInicio == null ? null : (DateTime?)DateTime.Parse(dInicio.ToString());
                nivelCine.dFechaFin = dFin == null ? null : (DateTime?)DateTime.Parse(dFin.ToString());

                this._listNivelAc.Add(nivelCine);
            }

            return this._listNivelAc;
        }

        [Route("~/cinecampoamplio/add")]
        [HttpPost]
        public string Add(CineCampoAmplio cineCampoAmplio)
        {
            Member member = new Member();
            member.MemberId = new MemberIdentifier()
            {
                Name = cineCampoAmplio.sNombre,
                Code = cineCampoAmplio.sCodigo,
                MemberType = MemberType.Leaf,
            };
            IDictionary<string, object> attributes =
            new Dictionary<string, object>();
            attributes.Add(descripcion, cineCampoAmplio.sDescripcion);
            attributes.Add(fechainicio, cineCampoAmplio.dFechaInicio);
            attributes.Add(fechafin, cineCampoAmplio.dFechaFin);
            member.Attributes = new List<MdsWebService.Attribute>();
            member.Attributes = this._entity.setAttributes(attributes);
            var result = this._entity.AddMember(member);

            return result;
        }

        [Route("~/cinecampoamplio/update")]
        [HttpPost]
        public string Update(CineCampoAmplio cineCampoAmplio)
        {
            Member member = new Member();
            member.MemberId = new MemberIdentifier()
            {
                Name = cineCampoAmplio.sNombre,
                Code = cineCampoAmplio.sCodigo,
                MemberType = MemberType.Leaf,
            };
            IDictionary<string, object> attributes =
            new Dictionary<string, object>();
            attributes.Add(descripcion, cineCampoAmplio.sDescripcion);
            attributes.Add(fechainicio, cineCampoAmplio.dFechaInicio);
            attributes.Add(fechafin, cineCampoAmplio.dFechaFin);
            member.Attributes = new List<MdsWebService.Attribute>();
            member.Attributes = this._entity.setAttributes(attributes);
            var result = this._entity.UpdateMember(member);
            return result;
        }


        [Route("~/cinecampoamplio/delete")]
        [HttpPost]
        public string Delete(CineCampoAmplio cineCampoAmplio)
        {
            var result = this._entity.DeleteMember(cineCampoAmplio.sCodigo);
            return result;
        }


    }
}