﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MDS.Utilities;
using MdsWebService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WSMetodologia.Models;

namespace WSMetodologia.Controllers
{
    [Produces("application/json")]
    [Route("[controller]")]
    [ApiController]
    public class MetotologiaController : ControllerBase
    {
        private MDSEntity _entity { get; set; }
        private List<Metodologia> _listNivelAc { get; set; }


        #region Constantes

        private const string entidad = "METODOLOGIA";
        private const string name = "Name";
        private const string code = "Code";
        private const string descripcion = "DESCRIPCION";
        private const string fechainicio = "FECHA_INICIO";
        private const string fechafin = "FECHA_FIN";
        private const string idniveleducativo = "ID_NIVEL_EDUCATIVO";

        #endregion

        public MetotologiaController()
        {
            this._entity = new MDSEntity(entidad);

        }

        [HttpGet]
        public JsonResult Get()
        {
            return new JsonResult("Datos de metodologia");
        }

        [HttpPost]
        public List<Metodologia> Post(Metodologia metodologia = null)
        {
            string searchTerm = null;

            if (metodologia != null)
            {
                IDictionary<string, object> attributes =
                new Dictionary<string, object>();
                attributes.Add(code, metodologia.sCodigo);
                attributes.Add(name, metodologia.sNombre);
                attributes.Add(descripcion, metodologia.sDescripcion);
                attributes.Add(fechainicio, metodologia.dFechaInicio);
                attributes.Add(fechafin, metodologia.dFechaFin);

                searchTerm = this._entity.searchTermAttributes(attributes);
            }

            List<Member> result = this._entity.GetEntity(searchTerm);
            this._listNivelAc = new List<Metodologia>();
            foreach (Member member in result)
            {
                Metodologia nivelMetodologia = new Metodologia();
                nivelMetodologia.sNombre = member.MemberId.Name;
                nivelMetodologia.sCodigo = member.MemberId.Code;
                nivelMetodologia.sDescripcion = member.Attributes.Single(i => i.Identifier.Name.Equals(descripcion)).Value.ToString();

                var dInicio = member.Attributes.Single(i => i.Identifier.Name.Equals(fechainicio)).Value;
                var dFin = member.Attributes.Single(i => i.Identifier.Name.Equals(fechafin)).Value;
                nivelMetodologia.dFechaInicio = dInicio == null ? null : (DateTime?)DateTime.Parse(dInicio.ToString());
                nivelMetodologia.dFechaFin = dFin == null ? null : (DateTime?)DateTime.Parse(dFin.ToString());

                var educativo = member.Attributes.Single(i => i.Identifier.Name.Equals(idniveleducativo)).Value;
                MemberIdentifier memberEducativo = (MemberIdentifier)educativo;
                nivelMetodologia.idNivelEducativo.sCodigo = memberEducativo.Code;

                this._listNivelAc.Add(nivelMetodologia);
            }

            return this._listNivelAc;
        }


        [Route("~/metodologia/add")]
        [HttpPost]
        public string Add(Metodologia metodologia)
        {
            Member member = new Member();
            member.MemberId = new MemberIdentifier()
            {
                Name = metodologia.sNombre,
                Code = metodologia.sCodigo,
                MemberType = MemberType.Leaf,
            };
            IDictionary<string, object> attributes =
            new Dictionary<string, object>();
            attributes.Add(descripcion, metodologia.sDescripcion);
            attributes.Add(fechainicio, metodologia.dFechaInicio);
            attributes.Add(fechafin, metodologia.dFechaFin);

            if (metodologia.idNivelEducativo != null)
            {
                MemberIdentifier memberIdentifier = new MemberIdentifier();
                memberIdentifier.Code = metodologia.idNivelEducativo.sCodigo;
                attributes.Add(idniveleducativo, memberIdentifier);
            }

            member.Attributes = new List<MdsWebService.Attribute>();
            member.Attributes = this._entity.setAttributes(attributes);
            var result = this._entity.AddMember(member);

            return result;
        }

        [Route("~/metodologia/update")]
        [HttpPost]
        public string Update(Metodologia metodologia)
        {
            Member member = new Member();
            member.MemberId = new MemberIdentifier()
            {
                Name = metodologia.sNombre,
                Code = metodologia.sCodigo,
                MemberType = MemberType.Leaf,
            };
            IDictionary<string, object> attributes =
            new Dictionary<string, object>();
            attributes.Add(descripcion, metodologia.sDescripcion);
            attributes.Add(fechainicio, metodologia.dFechaInicio);
            attributes.Add(fechafin, metodologia.dFechaFin);

            if (metodologia.idNivelEducativo != null)
            {
                MemberIdentifier memberIdentifier = new MemberIdentifier();
                memberIdentifier.Code = metodologia.idNivelEducativo.sCodigo;
                attributes.Add(idniveleducativo, memberIdentifier);
            }

            member.Attributes = new List<MdsWebService.Attribute>();
            member.Attributes = this._entity.setAttributes(attributes);
            var result = this._entity.UpdateMember(member);
            return result;
        }

        [Route("~/nivelacademico/delete")]
        [HttpPost]
        public string Delete(Metodologia metodologia)
        {
            var result = this._entity.DeleteMember(metodologia.sCodigo);
            return result;
        }


    }
}