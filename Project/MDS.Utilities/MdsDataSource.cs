﻿using System;
using System.Collections.Generic;
using System.IO; 
using System.Security.Principal;
using System.ServiceModel;
using System.Text;
using MdsWebService;
using Microsoft.Extensions.Configuration;

using System.Runtime.Serialization;
using System.ServiceModel.Description;

namespace MDS.Utilities
{

    public sealed class MdsDataSource
    {

        private ServiceClient _client;


        public string MdsServiceLocation { get; set; }


        public MdsDataSource()
         : base()
        {
        }

        internal ServiceClient Client
        {
            get
            {
                var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json").Build();
                string urlMds = config["Urlmds"];

                string domain = config["DomainMDS"];
                string username = config["UserNameMDS"];
                string password = config["PasswordMDS"];


               // string autenticathion = config["HttpClientCredentialType"];

                    var binding = new BasicHttpBinding();
                    binding.MaxReceivedMessageSize = 1024 * 1024 * 5;
                    binding.SendTimeout = TimeSpan.FromMinutes(2);
                    binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
                    binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;

                   var ep = new EndpointAddress(new Uri(urlMds ));
                    _client = new ServiceClient(binding, ep);


                    _client.ClientCredentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Impersonation;

                    _client.ClientCredentials.Windows.ClientCredential.Domain = domain;
                    _client.ClientCredentials.Windows.ClientCredential.UserName =  username;
                    _client.ClientCredentials.Windows.ClientCredential.Password = password;


                return _client;
            }
             
        }


 




    }
}
