﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MDS.Utilities.Entities
{
    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
