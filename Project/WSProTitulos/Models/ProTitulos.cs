﻿using MDS.Utilities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSProTitulos.Models
{
    public class ProTitulos
    {
        public User user { get; set; }

        public ProTitulos()
        {
            user = new User();
            idNivelAcademico = new Parametrica();
            idCineCampoAmplio = new Parametrica();
            idSistemaOrigen = new Parametrica();
        }

        public string sCodigo { get; set; }
        public string sNombre { get; set; }
        public Parametrica idNivelAcademico { get; set; }
        public string sDenominacion { get; set; }
        public Parametrica idCineCampoAmplio { get; set; }
        public Parametrica idSistemaOrigen { get; set; }
        public class Parametrica
        {
            public string sCodigo { get; set; }
        }
    }
}
