﻿using MdsWebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSProTitulos.Constants;
using WSProTitulos.Models;

namespace WSProTitulos.DTOs
{
    public class AutoMapperConfiguration : AutoMapper.Profile
    {

        public AutoMapperConfiguration()
        {
            CreateMap<Member, ProTitulos>()
                .ForMember(dest => dest.sCodigo, mo => mo.MapFrom(src => src.MemberId.Code))
                .ForMember(dest => dest.sNombre, mo => mo.MapFrom(src => src.MemberId.Name))
                .ForMember(dest => dest.sDenominacion, mo => mo.MapFrom(
                     src => src.Attributes.Single(i => i.Identifier.Name.Equals(constprotitulos.denominacion)).Value.ToString()
                   ))
                 
                ;

        }

    }
}
