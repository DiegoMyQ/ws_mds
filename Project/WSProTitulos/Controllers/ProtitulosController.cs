﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MDS.Utilities;
using MDS.Utilities.Services;
using MdsWebService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WSProTitulos.Constants;
using WSProTitulos.Models;

namespace WSProTitulos.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class ProtitulosController : ControllerBase
    {
        private MDSEntity _entity { get; set; }

        private List<ProTitulos> _listProTitulos { get; set; }

        private IUserService _userService;
        private readonly IMapper _mapper;

        public ProtitulosController(IUserService userService, IMapper mapper)
        {
            this._entity = new MDSEntity(constprotitulos.entidad);
            this._userService = userService;
            this._mapper = mapper;
        }

        [HttpGet]
        public JsonResult Get()
        {
            return new JsonResult("Datos de protitulos");
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Post(ProTitulos proTitulos = null)
        {
            try
            {
                //VALIDACION DE AUTENTICACION
                var user = _userService.Authenticate(proTitulos.user.Username, proTitulos.user.Password).Result;
                if (user == null)
                    return BadRequest(new { Mensaje = "Usuario o contraseña incorrecto", Errores = 1 });

                string searchTerm = null;

                if (proTitulos != null)
                {
                    IDictionary<string, object> attributes =
                    new Dictionary<string, object>();
                    attributes.Add(constprotitulos.code, proTitulos.sCodigo);
                    attributes.Add(constprotitulos.name, proTitulos.sNombre);
                    attributes.Add(constprotitulos.denominacion, proTitulos.sDenominacion);

                    searchTerm = this._entity.searchTermAttributes(attributes);
                }

                this._listProTitulos = new List<ProTitulos>();

                EntityMembersGetResponse result = this._entity.GetEntity(searchTerm);

                this._listProTitulos = _mapper.Map<List<ProTitulos>>(result.EntityMembers.Members);

                for (int i = 0; i < this._listProTitulos.Count; i++)
                {
                    var member = result.EntityMembers.Members[i];
                    var educativo = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constprotitulos.idnivelacademico)).Value;
                    MemberIdentifier memberEducativo = (MemberIdentifier)educativo;
                    this._listProTitulos[i].idNivelAcademico.sCodigo = memberEducativo.Code;

                    var cinecampoamplio = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constprotitulos.idcinecampoamplio)).Value;
                    MemberIdentifier memberCineCampoAmplio = (MemberIdentifier)cinecampoamplio;
                    this._listProTitulos[i].idCineCampoAmplio.sCodigo = memberCineCampoAmplio.Code;

                    var sistemaorigen = member.Attributes.Single(ie => ie.Identifier.Name.Equals(constprotitulos.idsistemaorigen)).Value;
                    MemberIdentifier memberSistemaOrigen = (MemberIdentifier)sistemaorigen;
                    this._listProTitulos[i].idSistemaOrigen.sCodigo = memberSistemaOrigen.Code;

                }

                int Errores = result.OperationResult.Errors.Count;
                string Mensaje = string.Empty;
                if (Errores > 0)
                    Mensaje = result.OperationResult.Errors.First().Description;
                else
                    Mensaje = "OK";

                return Ok(new { Mensaje, Errores, this._listProTitulos });


            }
            catch (Exception ex)
            {
                return BadRequest(new { Mensaje = "Error: "+ ex.Message, Errores = 1 });
            }

        }

        [AllowAnonymous]
        [Route("~/add")]
        [HttpPost]
        public ActionResult Add(ProTitulos proTitulos)
        {
            try
            {
                //VALIDACION DE AUTENTICACION
                var user = _userService.Authenticate(proTitulos.user.Username, proTitulos.user.Password).Result;
                if (user == null)
                    return BadRequest(new { Mensaje = "Usuario o contraseña incorrecto", Errores = 1 });


                Member member = new Member();
                member.MemberId = new MemberIdentifier()
                {
                    Name = proTitulos.sNombre,
                    //Code = institucion.sCodigo,
                    MemberType = MemberType.Leaf,
                };

                IDictionary<string, object> attributes = new Dictionary<string, object>();
                attributes.Add(constprotitulos.denominacion, proTitulos.sDenominacion);

                if (proTitulos.idNivelAcademico != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = proTitulos.idNivelAcademico.sCodigo;
                    attributes.Add(constprotitulos.idnivelacademico, memberIdentifier);
                }
                if (proTitulos.idCineCampoAmplio != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = proTitulos.idCineCampoAmplio.sCodigo;
                    attributes.Add(constprotitulos.idcinecampoamplio, memberIdentifier);
                }
                if (proTitulos.idSistemaOrigen != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = proTitulos.idSistemaOrigen.sCodigo;
                    attributes.Add(constprotitulos.idsistemaorigen, memberIdentifier);
                }
                member.Attributes = new List<MdsWebService.Attribute>();
                member.Attributes = this._entity.setAttributes(attributes);

                var result = this._entity.AddMember(member);

                int Errores = result.OperationResult.Errors.Count;
                string Mensaje = string.Empty;
                if (Errores > 0)
                    Mensaje = result.OperationResult.Errors.First().Description;
                else
                    Mensaje = "OK";

                int code = int.Parse(result.CreatedMembers.FirstOrDefault().Code);

                return Ok(new { Mensaje, Errores, proTitulo = code });

            }
            catch (Exception ex)
            {
                return BadRequest(new { Mensaje = "Error: " + ex.Message, Errores = 1 });
            }

        }

        [AllowAnonymous]
        [Route("~/update")]
        [HttpPost]
        public ActionResult Update(ProTitulos proTitulos)
        {
            try
            {
                //VALIDACION DE AUTENTICACION
                var user = _userService.Authenticate(proTitulos.user.Username, proTitulos.user.Password).Result;
                if (user == null)
                    return BadRequest(new { Mensaje = "Usuario o contraseña incorrecto", Errores = 1 });


                Member member = new Member();
                member.MemberId = new MemberIdentifier()
                {
                    Name = proTitulos.sNombre,
                    Code = proTitulos.sCodigo,
                    MemberType = MemberType.Leaf,
                };

                IDictionary<string, object> attributes = new Dictionary<string, object>();
                attributes.Add(constprotitulos.denominacion, proTitulos.sDenominacion);

                if (proTitulos.idNivelAcademico != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = proTitulos.idNivelAcademico.sCodigo;
                    attributes.Add(constprotitulos.idnivelacademico, memberIdentifier);
                }
                if (proTitulos.idCineCampoAmplio != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = proTitulos.idCineCampoAmplio.sCodigo;
                    attributes.Add(constprotitulos.idcinecampoamplio, memberIdentifier);
                }
                if (proTitulos.idSistemaOrigen != null)
                {
                    MemberIdentifier memberIdentifier = new MemberIdentifier();
                    memberIdentifier.Code = proTitulos.idSistemaOrigen.sCodigo;
                    attributes.Add(constprotitulos.idsistemaorigen, memberIdentifier);
                }
                member.Attributes = new List<MdsWebService.Attribute>();
                member.Attributes = this._entity.setAttributes(attributes);

                var result = this._entity.UpdateMember(member);

                int Errores = result.OperationResult.Errors.Count;
                string Mensaje = string.Empty;
                if (Errores > 0)
                    Mensaje = result.OperationResult.Errors.First().Description;
                else
                    Mensaje = "OK";

                return Ok(new { Mensaje, Errores });

            }
            catch (Exception ex)
            {
                return BadRequest(new { Mensaje = "Error: " + ex.Message, Errores = 1 });
            }

        }



        [AllowAnonymous]
        [Route("~/delete")]
        [HttpPost]
        public ActionResult Delete(ProTitulos proTitulos)
        {
            try
            {
                //VALIDACION DE AUTENTICACION
                var user = _userService.Authenticate(proTitulos.user.Username, proTitulos.user.Password).Result;
                if (user == null)
                    return BadRequest(new { Mensaje = "Usuario o contraseña incorrecto", Errores = 1 });

                var result = this._entity.DeleteMember(proTitulos.sCodigo);
                int Errores = result.OperationResult.Errors.Count;
                string Mensaje = string.Empty;
                if (Errores > 0)
                    Mensaje = result.OperationResult.Errors.First().Description;
                else
                    Mensaje = "OK";

                return Ok(new { Mensaje, Errores });

            }
            catch (Exception ex)
            {
                return BadRequest(new { Mensaje = "Error: " + ex.Message, Errores = 1 });
            }
        }



    }
}