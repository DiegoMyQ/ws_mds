﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSProTitulos.Constants
{
    public class constprotitulos
    {
        public const string entidad = "PROGRAMA_EXTRANJERO";
        public const string name = "Name";
        public const string code = "Code";
        public const string idnivelacademico = "NIVEL_ACADEMICO";
        public const string denominacion = "DENOMINACION";
        public const string idcinecampoamplio = "ID_CINE_CAMPO_AMPLIO";
        public const string idsistemaorigen = "ID_SISTEMA_ORIGEN";

    }
}
