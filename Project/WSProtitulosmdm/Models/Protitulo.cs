﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WSProtitulosmdm.Models
{

    [Table("PRO_TITULOS", Schema = "mdm")]
    public class Protitulos
    {
        [Key]
        public int ID_TITULO { get; set; }
        public int? ID_NIVEL_ACADEMICO { get; set; }
        public string DENOMINACION { get; set; }
        public int? ID_CINE_CAMPO_AMPLIO { get; set; }
        public int? ID_SISTEMA_ORIGEN { get; set; }

    }
}
