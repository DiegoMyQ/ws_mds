﻿using MDS.Utilities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSProtitulosmdm.Models
{
    public class ProtituloView
    {
        public User user { get; set; }

        public ProtituloView()
        {
            user = new User();
        }

        public int sCodigo { get; set; }
        public int? IdNivelAcademico { get; set; }
        public string sDenominacion { get; set; }
        public int? IdCineAmplio { get; set; }
        public int? IdSistemaOrigen { get; set; }


    }
}
