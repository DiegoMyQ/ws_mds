﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MDS.Utilities.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WSProtitulosmdm.Models;
using WSProtitulosmdm.Repositories;

namespace WSProtitulosmdm.Controllers
{

    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class ProtituloController : ControllerBase
    {

        private readonly IProtituloRepository _protituloRepository;
        private List<ProtituloView> _listProtitulos;

        private IUserService _userService;
        private readonly IMapper _mapper;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ProtituloController));


        public ProtituloController(IProtituloRepository protituloRepository, IUserService userService, IMapper mapper)
        {
            this._protituloRepository = protituloRepository;
            this._userService = userService;
            this._mapper = mapper;
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Post(ProtituloView protitulo = null)
        {
            try
            {
                string jsonData = JsonConvert.SerializeObject(protitulo);
                log.Info("Información Ingresada: " + jsonData);
                //VALIDACION DE AUTENTICACION
                var user = _userService.Authenticate(protitulo.user.Username, protitulo.user.Password).Result;
                if (user == null)
                {
                    log.Warn("Usuario o contraseña incorrecto");
                    return BadRequest(new { Mensaje = "Usuario o contraseña incorrecto", Errores = 1 });
                }

                this._listProtitulos = new List<ProtituloView>();
                var p = _mapper.Map<Protitulos>(protitulo);
                var result = this._protituloRepository.GetAllProtitulos(p);

                this._listProtitulos = _mapper.Map<List<ProtituloView>>(result);

                int Errores = 0;
                string Mensaje = string.Empty;

                return Ok(new { Mensaje, Errores, this._listProtitulos });
            }
            catch (Exception ex)
            {
                log.Error("Error en el metodo actualizarTrazDocumento" + ex);
                log.Error("Detalle del error : " + ex.Message);
                log.Error("InnerException: " + ex.InnerException);
                log.Error("Source: " + ex.Source);
                log.Error("HelpLink: " + ex.HelpLink);
                log.Error("StackTrace: " + ex.StackTrace);
                log.Error("HResult: " + ex.HResult);
                log.Error("TargetSite: " + ex.TargetSite.Name + ex.TargetSite.Module);
                log.Error("TargetSite: " + ex.Data.Keys);
                log.Error("Tipo de excepción: " + ex.GetType().FullName);
                return BadRequest(new { Mensaje = "Error: " + ex.Message, Errores = 1 });
            }
        }

        [AllowAnonymous]
        [Route("~/add")]
        [HttpPost]
        public ActionResult Add(ProtituloView protitulo = null)
        {
            try {

                //VALIDACION DE AUTENTICACION
                string jsonData = JsonConvert.SerializeObject(protitulo);
                log.Info("Información Ingresada: " + jsonData);
                //VALIDACION DE AUTENTICACION
                var user = _userService.Authenticate(protitulo.user.Username, protitulo.user.Password).Result;
                if (user == null)
                {
                    log.Warn("Usuario o contraseña incorrecto");
                    return BadRequest(new { Mensaje = "Usuario o contraseña incorrecto", Errores = 1 });
                }

                var p = _mapper.Map<Protitulos>(protitulo);
                int result = this._protituloRepository.Add(p);

                int Errores = 0;
                string Mensaje = string.Empty;
                if (result > 0)
                    Mensaje = "OK";
                else
                {
                    Mensaje = "Se presento un problema al crear intentelo nuevamente.";
                    Errores = 1;
                }

                return Ok(new { Mensaje, Errores, protitulo = result });
            }
            catch (Exception ex)
            {
                log.Error("Error en el metodo actualizarTrazDocumento" + ex);
                log.Error("Detalle del error : " + ex.Message);
                log.Error("InnerException: " + ex.InnerException);
                log.Error("Source: " + ex.Source);
                log.Error("HelpLink: " + ex.HelpLink);
                log.Error("StackTrace: " + ex.StackTrace);
                log.Error("HResult: " + ex.HResult);
                log.Error("TargetSite: " + ex.TargetSite.Name + ex.TargetSite.Module);
                log.Error("TargetSite: " + ex.Data.Keys);
                log.Error("Tipo de excepción: " + ex.GetType().FullName);
                return BadRequest(new { Mensaje = "Error: " + ex.Message, Errores = 1 });
            }
        }

        [AllowAnonymous]
        [Route("~/update")]
        [HttpPost]
        public ActionResult Update(ProtituloView protitulo = null)
        {
            try {

                //VALIDACION DE AUTENTICACION
                string jsonData = JsonConvert.SerializeObject(protitulo);
                log.Info("Información Ingresada: " + jsonData);
                //VALIDACION DE AUTENTICACION
                var user = _userService.Authenticate(protitulo.user.Username, protitulo.user.Password).Result;
                if (user == null)
                {
                    log.Warn("Usuario o contraseña incorrecto");
                    return BadRequest(new { Mensaje = "Usuario o contraseña incorrecto", Errores = 1 });
                }

                var p = _mapper.Map<Protitulos>(protitulo);

                int result = this._protituloRepository.Update(p);

                int Errores = 0;
                string Mensaje = string.Empty;

                if (result > 0)
                    Mensaje = "OK";
                else
                {
                    Mensaje = "Se presento un problema al crear intentelo nuevamente.";
                    Errores = 1;
                }

                return Ok(new { Mensaje, Errores, protitulo = result });
            }
            catch (Exception ex)
            {
                log.Error("Error en el metodo actualizarTrazDocumento" + ex);
                log.Error("Detalle del error : " + ex.Message);
                log.Error("InnerException: " + ex.InnerException);
                log.Error("Source: " + ex.Source);
                log.Error("HelpLink: " + ex.HelpLink);
                log.Error("StackTrace: " + ex.StackTrace);
                log.Error("HResult: " + ex.HResult);
                log.Error("TargetSite: " + ex.TargetSite.Name + ex.TargetSite.Module);
                log.Error("TargetSite: " + ex.Data.Keys);
                log.Error("Tipo de excepción: " + ex.GetType().FullName);
                return BadRequest(new { Mensaje = "Error: " + ex.Message, Errores = 1 });
            }
        }

        [AllowAnonymous]
        [Route("~/delete")]
        [HttpPost]
        public ActionResult Delete(ProtituloView protitulo = null)
        {
            try
            {
                //VALIDACION DE AUTENTICACION
                string jsonData = JsonConvert.SerializeObject(protitulo);
                log.Info("Información Ingresada: " + jsonData);
                //VALIDACION DE AUTENTICACION
                var user = _userService.Authenticate(protitulo.user.Username, protitulo.user.Password).Result;
                if (user == null)
                {
                    log.Warn("Usuario o contraseña incorrecto");
                    return BadRequest(new { Mensaje = "Usuario o contraseña incorrecto", Errores = 1 });
                }
                 
                var p = _mapper.Map<Protitulos>(protitulo);

                int result = this._protituloRepository.Delete(p);

                int Errores = 0;
                string Mensaje = string.Empty;

                if (result > 0)
                    Mensaje = "OK";
                else
                {
                    Mensaje = "Se presento un problema al crear intentelo nuevamente.";
                    Errores = 1;
                }

                return Ok(new { Mensaje, Errores, protitulo = result });

            }
            catch (Exception ex)
            {
                log.Error("Error en el metodo actualizarTrazDocumento" + ex);
                log.Error("Detalle del error : " + ex.Message);
                log.Error("InnerException: " + ex.InnerException);
                log.Error("Source: " + ex.Source);
                log.Error("HelpLink: " + ex.HelpLink);
                log.Error("StackTrace: " + ex.StackTrace);
                log.Error("HResult: " + ex.HResult);
                log.Error("TargetSite: " + ex.TargetSite.Name + ex.TargetSite.Module);
                log.Error("TargetSite: " + ex.Data.Keys);
                log.Error("Tipo de excepción: " + ex.GetType().FullName);
                return BadRequest(new { Mensaje = "Error: "+ ex.Message, Errores = 1 });
            }
        }

    }
}