﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSProtitulosmdm.Models;

namespace WSProtitulosmdm.Repositories
{
    public interface IProtituloRepository
    {
        List<Protitulos> GetAllProtitulos(Protitulos protitulos);
        int Add(Protitulos protitulos);
        int Update(Protitulos protitulos);
        int Delete(Protitulos protitulos);
        Protitulos FindAsync(int codigo);
    }
}
