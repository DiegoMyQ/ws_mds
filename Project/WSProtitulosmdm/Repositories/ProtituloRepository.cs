﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSProtitulosmdm.Context;
using WSProtitulosmdm.Models;

namespace WSProtitulosmdm.Repositories
{
    public class ProtituloRepository: IProtituloRepository
    {
        private readonly IDbContext _context;


        public ProtituloRepository(IDbContext context)
        {
            _context = context;
        }

        public List<Protitulos> GetAllProtitulos(Protitulos protitulos)
        {
            var query = _context.Protitulos;
            return query.ToList(); ;
        }

        public  int Add(Protitulos protitulos)
        {
            var p = GetProtituloLastOrDefault();
            protitulos.ID_TITULO = p.ID_TITULO + 1;
            _context.Protitulos.Add(protitulos); 
            int result = _context.SaveChangesAsync().Result;
            return protitulos.ID_TITULO;
        }

        public int Update(Protitulos protitulos)
        {
            /*var p =  FindAsync(protitulos.ID_TITULO);

            if (p == null)
            {
                throw new Exception("El protitulo a actualizar no existe. Por favor valide");
            }*/

            _context.Protitulos.Update(protitulos);
            int result = _context.SaveChangesAsync().Result;
 

            return result;
        }

        public int Delete(Protitulos protitulos)
        {
            var p = _context.Protitulos.FindAsync(protitulos.ID_TITULO);

            /*if (p.Result == null)
            {
                throw new Exception("El protitulo a eliminar no existe. Por favor valide");
            }*/
             

            _context.Protitulos.Remove(protitulos);
            int result = _context.SaveChangesAsync().Result;
            return result;
        }


        public Protitulos GetProtituloLastOrDefault()
        {
            var query = _context.Protitulos.ToList().LastOrDefault();
            return query; ;
        }

        public Protitulos FindAsync(int codigo)
        {
            var p = _context.Protitulos.FindAsync(codigo);
            return p.Result; ;
        }

    }
}
