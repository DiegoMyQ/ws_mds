﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSProtitulosmdm.Models;

namespace WSProtitulosmdm.DTOs
{
    public class AutoMapperConfiguration : AutoMapper.Profile
    {

        public AutoMapperConfiguration()
        {
            CreateMap<Protitulos, ProtituloView>()
              .ForMember(dest => dest.sCodigo, mo => mo.MapFrom(src => src.ID_TITULO))
              .ForMember(dest => dest.sDenominacion, mo => mo.MapFrom(src => src.DENOMINACION))
              .ForMember(dest => dest.IdNivelAcademico, mo => mo.MapFrom(src => src.ID_NIVEL_ACADEMICO))
              .ForMember(dest => dest.IdCineAmplio, mo => mo.MapFrom(src => src.ID_CINE_CAMPO_AMPLIO))
              .ForMember(dest => dest.IdSistemaOrigen, mo => mo.MapFrom(src => src.ID_SISTEMA_ORIGEN))
              ;


            CreateMap<ProtituloView,Protitulos>()
              .ForMember(dest => dest.ID_TITULO, mo => mo.MapFrom(src => src.sCodigo))
              .ForMember(dest => dest.DENOMINACION, mo => mo.MapFrom(src => src.sDenominacion))
              .ForMember(dest => dest.ID_NIVEL_ACADEMICO, mo => mo.MapFrom(src => src.IdNivelAcademico))
              .ForMember(dest => dest.ID_CINE_CAMPO_AMPLIO, mo => mo.MapFrom(src => src.IdCineAmplio))
              .ForMember(dest => dest.ID_SISTEMA_ORIGEN, mo => mo.MapFrom(src => src.IdSistemaOrigen))
              ;

        }




    }
}
