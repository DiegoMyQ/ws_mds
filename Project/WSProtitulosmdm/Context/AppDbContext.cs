﻿using Microsoft.EntityFrameworkCore;
using WSProtitulosmdm.Models;

namespace WSProtitulosmdm.Context
{
    public partial class AppDbContext : DbContext, IDbContext
    {

        private readonly DbContextOptions<AppDbContext> _contextOptions;
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            _contextOptions = options;
        }


        public DbSet<Protitulos> Protitulos { get; set; }

    }
}
